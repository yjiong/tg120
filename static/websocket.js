(function (window, undefined) {
    $(function () {
        var socket, $win = $('body');
	showmessage = function (msg, type) {
            var datetime = new Date();
            var tiemstr = datetime.getHours() + ':' + datetime.getMinutes() + ':' + datetime.getSeconds() + '.' + datetime.getMilliseconds();
            if (type) {
                var $p = $('<div>').appendTo($win.find('#div_msg'));
                var $type = $('<span>').text('[' + tiemstr + ']' + type + '：').appendTo($p);
		try {
		    msg_obj = JSON.parse(msg);
		    fmt_msg = JSON.stringify(msg_obj, null, 2);
		}
		catch(e) {
		    fmt_msg = msg;
		}
                var $msg = $('<pre>').addClass('thumbnail').css({ 'margin-bottom': '5px' }).text(fmt_msg).appendTo($p);
            } else {
                var $center = $('<center>').text(msg + '(' + tiemstr + ')').css({ 'font-size': '12px' }).appendTo($win.find('#div_msg'));
            }
        };
        $win.find('#refresh_clearcache').click(function () {
            $.yszrefresh();
        });
        
        var url = "ws" + document.location.href.substring(4) + "/message";
        socket = new WebSocket(url);
        socket.onmessage = function (eve) {
            showmessage(eve.data, 'receive');
        };
        
        socket.onopen = function (event) {
            showmessage('连接成功');
        };
        socket.onclose = function (event) {
            showmessage('断开连接');
        };
/**************************************
        $win.find('#btn_conn').attr('disabled', false);
       $win.find('#btn_close').attr('disabled', true);

        $win.find('#btn_conn').click(function () {
            $win.find('#btn_conn').attr('disabled', true);
            $win.find('#btn_close').attr('disabled', false);
            //var url = $win.find('#inp_url').val();
            var url = "ws://127.0.0.1:8000/message";
            // 创建一个Socket实例
            socket = new WebSocket(url);
            showmessage('开始连接');
            // 打开Socket 
            socket.onopen = function (event) {
                // 发送一个初始化消息
                showmessage('连接成功');
            };
            // 监听消息
            socket.onmessage = function (eve) {
                showmessage(eve.data, 'receive');
            };
            // 监听Socket的关闭
            socket.onclose = function (event) {
                showmessage('断开连接');
                $win.find('#btn_conn').attr('disabled', false);
                $win.find('#btn_close').attr('disabled', true);
            };
        });*********************************/
        $win.find('#btn_close').click(function () {
            if (socket) {
                socket.close();
            }
        });
        $win.find('#btn_send').click(function () {
            var msg = $win.find('#inp_send').val();
            if (socket && msg) {
                socket.send(msg);
                showmessage(msg, 'send');
                $win.find('#inp_send').val('');
            }
        });
        $win.find('#inp_send').keyup(function () {
            if (event.ctrlKey && event.keyCode == 13) {
                $win.find('#btn_send').trigger('click');
            }
        });

        $win.find('#btn_clear').click(function () {
            $win.find('#div_msg').empty();
        }); 
    });
})(window);
