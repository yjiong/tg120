#!/usr/bin/python
#encoding:UTF-8
import base
from base import DynApp,DevObj 
from yj_global import yj_debugging 
import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
import json
from MyError import *
_debug = 0
# _log = base.ModuleLogger(globals())

@DynApp.registerdev(DynApp,'TC100-R8')
@yj_debugging
class TC100_R8(DevObj):
    def __init__(self):
        if _debug: TC100_R8._logger.setLevel(10);TC100_R8._debug("__init__ ")
        self.set_config()

    @classmethod
    def set_config(cls):
        cls.baudrate = 2400
        cls.bytesize = 8
        cls.parity = 'N'
        cls.stopbits = 1
        cls.timeout = 1 

################################---user code---################################
    def read_device(self,addr,ser):
        if _debug: TC100_R8._debug(r"read_device   addr:%-10rport:%r"%(addr,ser.port))
        device_value = {}
        master = modbus_rtu.RtuMaster(ser)
        master.set_timeout(1)
        try:
#             master.execute(1,cst.WRITE_MULTIPLE_REGISTERS,75,5,[0x000b,0x0102,0x0304,0x0506,0x0708])
            alldata = master.execute(int(addr), cst.READ_HOLDING_REGISTERS, 0, 80)
            mddata = alldata[72:80]
#            md = [hex(i) for i in mddata]
            date = "20%(year)02d-%(month)02d-%(day)02d %(hour)02d:%(min)02d:%(sec)02d"\
            % {'year':mddata[0] >> 8, 'month':mddata[0] & 255, 'day':mddata[1] >> 8, 'hour':mddata[1] & 255, \
              'min':mddata[2] >> 8, 'sec':mddata[2] & 255}

            light_stat = [];bitmove = 0x80
            delay_value = []
            for i in xrange(0, 8):
                if i % 2 == 0:
                    if mddata[4 + i / 2] >> 8 > 144:
                        delay_value.append(0)
                    else:
                        delay_value.append((mddata[4 + i / 2] >> 8) * 10)
                    if mddata[4 + i / 2] & 0xff > 144:
                        delay_value.append(0)
                    else:                        
                        delay_value.append((mddata[4 + i / 2] & 0xff) * 10)
                if mddata[3] / 0x100 & bitmove:
                    light_stat.append('ON')
                else:
                    light_stat.append('OFF')
                bitmove = bitmove >> 1
            control_state = []
            if mddata[3] & 0xff == 0x0b:
                for i in xrange(0, 4):
                    if mddata[4 + i] / 0x100 >= 144:
                        control_state.append('always_manual')
                    elif mddata[4 + i] / 0x100 < 144 and mddata[4 + i] / 0x100 > 0:
                        control_state.append('delay_manual')
                    elif mddata[4 + i] / 0x100 == 0:
                        control_state.append('automatic')
                    if mddata[4 + i] & 0xff >= 144:
                        control_state.append('always_manual')
                    elif mddata[4 + i] & 0xff < 144 and mddata[4 + i] & 0xff > 0:
                        control_state.append('delay_manual')
                    elif mddata[4 + i] & 0xff == 0:
                        control_state.append('automatic')
            else:
                for i in xrange(0, 8):
                    control_state.append('automatic')
            strate = alldata[0:72]
            temp_str = [None, None, None, None, None, None, None, None]
            for i in xrange(8):
                temp_str[i] = {'line_%s: ' % str(i + 1):''}
                for j in xrange(72):
                    if j % 3 == 0:
                        temp_str[i]['line_%s: ' % str(i + 1)] += ' %s:00=' % str(j / 3)      
                    if int(bin(strate[j]).split('0b')[1].zfill(16)[i]):
                        temp_str[i]['line_%s: ' % str(i + 1)] += '+'
                    else:
                        temp_str[i]['line_%s: ' % str(i + 1)] += '-'
                    
                    if int(bin(strate[j]).split('0b')[1].zfill(16)[i + 8]):
                        temp_str[i]['line_%s: ' % str(i + 1)] += '+'
                    else:
                        temp_str[i]['line_%s: ' % str(i + 1)] += '-'
            device_value.update({'_localtime':date, \
                                 '_line:1-8':light_stat, \
                                 '_control_state:1-8':control_state, \
                                 '_delay_value:1-8':delay_value, \
                                 '_strate':temp_str})
        except:
            pass  # print 'device is not online'
        if _debug: TC100_R8._debug(r'the device data is %r' % device_value)
        return device_value
################################---user code---################################

###############################---user code---#################################
    def write_strate(self,ser,addr,var_value):
        if _debug: TC100_R8._debug(r"write_val     port:%-10raddr:%-10rvalue:%r"%(ser.port,addr,var_value))
        ret = 0        
        try:
            master = modbus_rtu.RtuMaster(ser)
            master.set_timeout(1)
            alldata = master.execute(int(addr), cst.READ_HOLDING_REGISTERS,0,80)
            wstrate = list(alldata[0:72])
            for i in xrange(1,9):
                key = 'line_%s'%i
                for da in var_value:
                    if da.has_key(key):
                        bitcount = 0
                        for bitstr in da[key]:
                            if bitstr == '+':
                                movbit = 0x80
                                movbit >>= (i-1)
                                if bitcount % 2 == 0:
                                    wstrate[bitcount/2] |= movbit * 0x100
                                else:
                                    wstrate[bitcount/2] |= movbit
                                bitcount += 1
                                 
                            if bitstr == '-':
                                movbit = 0x80
                                movbit >>= (i-1)
                                if bitcount % 2 == 0:
                                    wstrate[bitcount/2] &= ((0xff - movbit) * 0x100 + 0xff)
                                else:
                                    wstrate[bitcount/2] &= (0xff00 + (0xff - movbit ))
                                bitcount += 1
                        if bitcount != 144:
                            return 'wrong data' 
            time = alldata[72:75]
            wstrate += time
            wstrate += [0xA0]
#             for i in xrange(72):
#                 print '%-30s %d '%(bin(wstrate[i]),i)
            master.execute(int(addr),cst.WRITE_MULTIPLE_REGISTERS,0,76,wstrate)
            return ret
        except:
            return 201
            
################################---user code---################################

###############################---user code---#################################
    def calibration_time(self,ser,addr,var_value):
        if _debug: TC100_R8._debug(r"Calibration_time     port:%-10raddr:%-10rvalue:%r"%(ser.port,addr,var_value))
        ret = 0       
        try:
            master = modbus_rtu.RtuMaster(ser)
            master.set_timeout(1)
            if isinstance(var_value, dict) and var_value.has_key('date') and var_value.has_key('time'):
                # var_value format :{'date':'12/31/2016','time':'10:35:55'}
                import re
                pattern_1 = re.compile(r'(\d{2})/(\d{2})/\d{2}(\d{2})')
                pattern_2 = re.compile(r'(\d{2}):(\d{2}):(\d{2})')            
                if  re.match(r'\d{2}/\d{2}/\d{4}',var_value['date']) and re.match(r'\d{2}:\d{2}:\d{2}',var_value['time']):
                    t1 = pattern_1.match(var_value['date']).groups()
                    t2 = pattern_2.match(var_value['time']).groups()
                    this_time = [int(t1[2]) * 0x100 + int(t1[0]),int(t1[1]) * 0x100 + int(t2[0]),int(t2[1]) * 0x100 + int(t2[2]),0xB0]
                    master.execute(int(addr),cst.WRITE_MULTIPLE_REGISTERS,72,4,this_time)
                    return 0
                else:
                    return "wrong format"
            else:
                return 'wrong data'
            return ret
        except:
            return 201
################################---user code---################################

###############################---user code---#################################
    def write_control(self,ser,addr,var_value):
        if _debug: TC100_R8._debug(r"write_control    port:%-10raddr:%-10rvalue:%r"%(ser.port,addr,var_value))
        ret = 0
        try:
            master = modbus_rtu.RtuMaster(ser)
            master.set_timeout(1)
            #var_value format :[{'line':1,'contr_mod':'delay_manual','on_off':'on','delay':20},{'line':2,'contr_mod':'always_manual','on_off':'on'}]
            this_ctrl = [0x0B,0xffff,0xffff,0xffff,0xffff]
            insbyte = 0x00
            for value in var_value:
                if isinstance(value, dict) and value.has_key('line')\
                 and value.has_key('contr_mod') and value.has_key('on_off')\
                 and value['line'] > 0 and value['line'] < 9\
                 and value['contr_mod'] in ['always_manual','delay_manual']:
                    movbit = 0x80
                    if value['on_off'] == r'on':
                        movbit >>= int(value['line'])-1
                        insbyte = insbyte | movbit
                    elif value['on_off'] == r'off':
                        movbit >>= int(value['line'])-1
                        insbyte = insbyte & (0xFF - movbit)
                    if value['contr_mod'] == 'always_manual':
                        if int(value['line']) % 2:
                            this_ctrl[int(value['line']) / 2 + int(value['line']) % 2] &= 0xEEff
                        else:
                            this_ctrl[int(value['line']) / 2 + int(value['line']) % 2] &= 0xffEE
                    else:
                        if value.has_key('delay'):
                            if int(value['line']) % 2:
                                this_ctrl[int(value['line']) / 2 + int(value['line']) % 2] &= (int(value['delay'])/10 * 0x100 + 0xff)
                            else:
                                this_ctrl[int(value['line']) / 2 + int(value['line']) % 2] &= (int(value['delay'])/10 + 0xff00)
                        else:
                            return 'wrong format'    
    
    #                 else:
    #                     return "wrong format"
                else:
                    return 'wrong data'
            this_ctrl[0] |= insbyte * 0x100
#             print [hex(i) for i in this_ctrl]
#             print [bin(i) for i in this_ctrl]
            master.execute(int(addr),cst.WRITE_MULTIPLE_REGISTERS,75,5,this_ctrl)
            return ret
        except:
            return 201
################################---user code---################################



if __name__ == '__main__':
    _debug = 0
    mydev = TC100_R8()
    myser = mydev._getser("/dev/ttyUSB0",0) 
    addr = 1
    value = [{"line_1" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_2" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_3" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_4" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_5" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_6" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_7" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'},
             {"line_8" : '0:00=++++++ 1:00=++++++ 2:00=++++++ 3:00=++++++ 4:00=++++++ 5:00=++++++ 6:00=++++++ \
    7:00=++++++ 8:00=++++++ 9:00=++++++ 10:00=++++++ 11:00=++++++ 12:00=++++++ 13:00=++++++ 14:00=++++++ 15:00=++++++ 16:00=++++++ \
    17:00=++++++ 18:00=++++++ 19:00=++++++ 20:00=++++++ 21:00=++++++ 22:00=++++++ 23:00=++++++'}]

             
#              {"line_5" : '0:00=------ 1:00=------ 2:00=------ 3:00=------ 4:00=------ 5:00=------ 6:00=------ \
#              7:00=------ 8:00=------ 9:00=------ 10:00=------ 11:00=------ 12:00=------ 13:00=------ 14:00=------ 15:00=------ \
#              16:00=------ 17:00=------ 18:00=------ 19:00=------ 20:00=------ 21:00=------ 22:00=------ 23:00=------'}]
      
      
#     value = [0,0,0,0,0,0,0,0,0,0,\
#              0,0,0,0,0,0,0,0,0,0,\
#              0,0,0,0,0,0,0,0,0,0,\
#              0,0,0,0,0,0,0,0,0,0,\
#              0,0,0,0,0,0,0,0,0,0,\
#              0,0,0,0,0,0,0,0,0,0,\
#              0,0,0,0,0,0,0,0,0,0,\
#            0,0]
#     print mydev.write_strate(myser,addr,value)
#     cvalue = {'date':'12/03/2016','time':'10:35:55'}
#     mydev.calibration_time(myser,addr,cvalue)
    contr = [{'line':1,'contr_mod':'delay_manual','on_off':'off','delay':20},{'line':2,'contr_mod':'always_manual','on_off':'off','delay':20},\
             {'line':3,'contr_mod':'delay_manual','on_off':'on','delay':0},{'line':4,'contr_mod':'always_manual','on_off':'off','delay':20},\
             {'line':5,'contr_mod':'always_manual','on_off':'off','delay':20},{'line':6,'contr_mod':'delay_manual','on_off':'on','delay':80},\
             {'line':7,'contr_mod':'delay_manual','on_off':'off','delay':58},{'line':8,'contr_mod':'delay_manual','on_off':'on','delay':20}]
    print mydev.write_control(myser,addr,contr)
#     print   json.dumps(mydev.read_device(addr, myser),indent = True)
    
    
    
    
    
    
    
    
