#!/usr/bin/python
# encoding: UTF-8
import time
from base import DynApp,DevObj 
from yj_global import yj_debugging
#from itertools import count
from MyError import *
_debug = 0

@DynApp.registerdev(DynApp,'DTZY422')
@yj_debugging
class Dtzy422(DevObj):
    def __init__(self):
        self.set_config()
        if _debug: Dtzy422._debug("__init__ ")
        self.on_off = ['ON', 'OFF']
        self.owe_status = ['owe_run', 'remaining_run']
        self.alarm_status_value = ['normal', 'overloaded', 'remaining_low_alarm','overloaded and remaining_low_alarm',\
                              'overdraught_alarm','overloaded and overdraught_alarm','remaining_low and overdraught_alarm','overloaded and remaining_low and overdraught_alarm',\
                              'unknow','unknow','unknow','unknow','unknow','unknow','unknow','unknow']
    @classmethod
    def set_config(cls):
        cls.baudrate = 1200
        cls.bytesize = 8
        cls.parity = 'E'
        cls.stopbits = 1
        cls.timeout = 1 
        
    def read_device(self,addr,ser):
        # dang qian zong yong dian liang sheng yu dian lian    
        send_data = self.create_read_buf(addr, [0x9A, 0x23])
        ser.write(send_data)
        rece_data = []
        for k in range(26):
            intva = ord(ser.read(1))
            rece_data += [intva]
        chsum = 0    
        for chsumi in rece_data[0:24]:
            chsum = (chsum + chsumi) % 256
        if(chsum == rece_data[24]):                        
# #                        print rece_data[10:24]
            dqzydl = self.crackbuf(rece_data[10:24])
#                             print ('dang qian zong yong dian liang + shengyu dian lian'+`dqzydl`)
            zydl = dqzydl[2:6]
            zydl.reverse()
            zydl = float(((zydl[0] / 16) * 10 + zydl[0] % 16) * 10000 + \
                         ((zydl[1] / 16) * 10 + zydl[1] % 16) * 100 + \
                         ((zydl[2] / 16) * 10 + zydl[2] % 16) + \
                         float((zydl[3] / 16) * 10 + zydl[3] % 16) / 100)
            sydl = dqzydl[6:10]
            sydl.reverse()
            sydl = float(((sydl[0] / 16) * 10 + sydl[0] % 16) * 10000 + \
                         ((sydl[1] / 16) * 10 + sydl[1] % 16) * 100 + \
                         ((sydl[2] / 16) * 10 + sydl[2] % 16) + \
                         float((sydl[3] / 16) * 10 + sydl[3] % 16) / 100)
            tzydl = dqzydl[10:14]
            tzydl.reverse()
            tzydl = float(((tzydl[0] / 16) * 10 + tzydl[0] % 16) * 10000 + \
                         ((tzydl[1] / 16) * 10 + tzydl[1] % 16) * 100 + \
                         ((tzydl[2] / 16) * 10 + tzydl[2] % 16) + \
                         float((tzydl[3] / 16) * 10 + tzydl[3] % 16) / 100)                        
#             print zydl,'\n',sydl,'\n',tzydl
        else:
            print 'xxxxxx'                       
        # ji liang can shu
        time.sleep(0.05)    
        send_data = self.create_read_buf(addr, [0xDA, 0x6C])
        ser.write(send_data)
        rece_data = []
        for k in range(28):
            intva = ord(ser.read(1))
            rece_data += [intva]
        chsum = 0    
        for chsumi in rece_data[0:26]:
            chsum = (chsum + chsumi) % 256
        if(chsum == rece_data[26]):                                                
            jlcs = self.crackbuf(rece_data[10:26])                        
# #                        print ('ji liang can shu'+`jlcs`)
            bjdl = jlcs[2:4]
            bjdl.reverse()
            bjdl = int(((bjdl[0] / 16) * 10 + bjdl[0] % 16) * 100 + \
                         ((bjdl[1] / 16) * 10 + bjdl[1] % 16))
            bjfh = jlcs[4:6]
            bjfh.reverse()
            bjfh = float(((bjfh[0] / 16) * 10 + bjfh[0] % 16) + \
                         float((bjfh[1] / 16) * 10 + bjfh[1] % 16) / 100)                       
            yxtouzhi = jlcs[6:8]
            yxtouzhi.reverse()
            yxtouzhi = int(((yxtouzhi[0] / 16) * 10 + yxtouzhi[0] % 16) * 100 + \
                         ((yxtouzhi[1] / 16) * 10 + yxtouzhi[1] % 16))                        
            yxtjdl = jlcs[8:12]                        
            yxtjdl.reverse()
            yxtjdl = int(((yxtjdl[0] / 16) * 10 + yxtjdl[0] % 16) * 1000000 + \
                         ((yxtjdl[1] / 16) * 10 + yxtjdl[1] % 16) * 10000 + \
                         ((yxtjdl[2] / 16) * 10 + yxtjdl[2] % 16) * 100 + \
                         ((yxtjdl[3] / 16) * 10 + yxtjdl[3] % 16))                        
            bjddsj = jlcs[12:14]                        
            bjddsj.reverse()
            bjddsj = int(((bjddsj[0] / 16) * 10 + bjddsj[0] % 16) * 100 + \
                         ((bjddsj[1] / 16) * 10 + bjddsj[1] % 16))                                     
#             print bjdl,bjfh,yxtouzhi,yxtjdl,bjddsj
        else:
            print 'xxxxxx'
        # kai he zha kong zhi shi jian
        # time.sleep(0.1)
        '''
        send_data = create_read_buf(addr,[0xEA,0x6C])
        ser.write(send_data)
        rece_data = []
        for k in range(18):
            intva = ord(ser.read(1))
            rece_data += [intva]
        chsum = 0    
        for chsumi in rece_data[0:16]:
            chsum = (chsum + chsumi)%256
        if(chsum == rece_data[16]):
           
            kongzsj = crackbuf(rece_data[10:16])
            print ('kong zhi shi jian'+`kongzsj`)    
        else:
            raise MyError
        '''    
        # kong zhi zi
        time.sleep(0.05)
        send_data = self.create_read_buf(addr, [0xFA, 0x6C])
        ser.write(send_data)
        rece_data = []
        for k in range(17):
            intva = ord(ser.read(1))
            rece_data += [intva]
        chsum = 0    
        for chsumi in rece_data[0:15]:
            chsum = (chsum + chsumi) % 256
        if(chsum == rece_data[15]):                       
            kzzt = self.crackbuf(rece_data[10:15])
#             print ('kong zhi zi'+`kzzt`)                        
            kongzhizizhuangtai = kzzt[2]
#             print kzzt,kzzt[3]
            owe_remaining = self.owe_status[(kzzt[3] & 0x80) / 0x80]
            alarm_status = self.alarm_status_value[kzzt[3] & 0x0f]
            zhukaiguan_on_off = self.on_off[(kzzt[4] & 0x02) / 0x02];fuzhukaiguan_on_off = self.on_off[(kzzt[4] & 0x01)]                        
#             print kongzhizizhuangtai,'\n',owe_remaining,'\n',alarm_status,'\n',zhukaiguan_on_off,'\n',fuzhukaiguan_on_off
        else:
            print 'xxxxxx'
        '''
        #zui hou yi ci shou dian xin xi
        time.sleep(0.05)    
        send_data = create_read_buf(addr,[0x0A,0x6C])
        ser.write(send_data)
        rece_data = []
        for k in range(28):
            intva = ord(ser.read(1))
            rece_data += [intva]
        chsum = 0    
        for chsumi in rece_data[0:26]:
            chsum = (chsum + chsumi)%256
        if(chsum == rece_data[26]):                                                
            zuihycsd = crackbuf(rece_data[10:26])
##                        print ('zui hou yi ci shou dian xin xi'+`zuihycsd`)
            goudianwangdian = int(((zuihycsd[3]/16)*10+zuihycsd[3]%16)*100 +\
                         ((zuihycsd[2]/16)*10+zuihycsd[2]%16))
            goudianshijian = str(((zuihycsd[9]/16)*10+zuihycsd[9]%16)*100 +\
                         ((zuihycsd[8]/16)*10+zuihycsd[8]%16))\
                         +'-'+str((zuihycsd[7]/16)*10+zuihycsd[7]%16)\
                         +'-'+str((zuihycsd[6]/16)*10+zuihycsd[6]%16)
            goudianliang = float(((zuihycsd[13]/16)*10+zuihycsd[13]%16)*10000 +\
                         ((zuihycsd[12]/16)*10+zuihycsd[12]%16)*100+\
                                 ((zuihycsd[11]/16)*10+zuihycsd[11]%16)+\
                                 float((zuihycsd[10]/16)*10+zuihycsd[10]%16)/100)
            goudiancishu = int(((zuihycsd[15]/16)*10+zuihycsd[15]%16)*100 +\
                         ((zuihycsd[14]/16)*10+zuihycsd[14]%16))
##                        print goudianwangdian,'\n',goudianshijian,'\n',goudianliang,'\n',goudiancishu
          
        else:
            raise MyError
        '''    
        time.sleep(0.05)
        # mddata = master.execute(parame[i][6], cst.READ_HOLDING_REGISTERS, parame[i][7], parame[i][8])
        p_amm_va_list = {'_total_electricity_consumption':zydl\
                            , '_remaining_electricity':sydl\
                            , '_overdraught_electricity':tzydl\
                            , '_remaining_alarm':bjdl\
                            , '_overloaded_alarm':bjfh\
                            , '_permit_overdraught':yxtouzhi\
                            , '_permit_hoarding':yxtjdl\
                            , '_contrl_byte':kongzhizizhuangtai\
                            , '_owe_remaining':owe_remaining\
                            , '_alarm_status':alarm_status\
                            , '_master_switch':zhukaiguan_on_off\
                            , '_auxiliary_switch':fuzhukaiguan_on_off\
#                           , 'message_time':time.ctime()\
#                           ,'_dev_addr':addr\
                            }
        return p_amm_va_list
#     def read_device_prepay_amm(self,optname,parame):
#         p_amm_va_list = []
#         serport = self.get_serport_no()
#         if len(parame) < 21:
#             return []
#         for i in xrange(len(serport)):
#             if (serport[i] == parame[0]):
#                 break
#         while (yj_global.RECEIVE_notempty == True):
#             time.sleep(1)
#         if(yj_global.SER_BESY[i] == False):
#             ser = self.openser(parame)
#             if str(parame[10]) == yj_global.DEVICE_NAME[0]:
#                 for j in range(len(parame) - 20):
#                     if(len(yj_global.RECEIVE) != 0):
#                         break
#                     try:
#                         pattern_op = re.compile(r'(\d*).(.*)-(\d{1,2}$)')
#                         match_op = pattern_op.match(optname[j+20])
#                         self.read_prepay_amm_sd(i,match_op.group(3),parame[j + 20], p_amm_va_list, ser,match_op.group(2))
#                     except:
#     # #                    print 'device['+`parame[10:12],parame[i][j+20]`+']is not online'
#                         pattern_op = re.compile(r'(\d*).(.*)-(\d{1,2}$)')
#                         match_op = pattern_op.match(optname[j+20])
#                         p_amm_va_list.append({'_port':i,'_devid':match_op.group(2),'_commid':str(match_op.group(3)),'_status':'offline'})
#     # #                    print (p_amm_va_list)
#             ser.close()
#     #         print  yj_global.SER_BESY[i],'NO',i  
#             yj_global.SER_BESY[i] = False
#         return p_amm_va_list
    def create_read_buf(self,amm_addr, cmd):
        if(len(amm_addr) != 12):
            for i in xrange(12 - len(amm_addr)):
                amm_addr = '0' + amm_addr
        buf = [0xFE]
        buf.append(0xFE)
        buf.append(0x68)
        buf.append(int(amm_addr[10:12]) / 10 * 16 + int(amm_addr[10:12]) % 10)
        buf.append(int(amm_addr[8:10]) / 10 * 16 + int(amm_addr[8:10]) % 10)
        buf.append(int(amm_addr[6:8]) / 10 * 16 + int(amm_addr[6:8]) % 10)
        buf.append(int(amm_addr[4:6]) / 10 * 16 + int(amm_addr[4:6]) % 10)
        buf.append(int(amm_addr[2:4]) / 10 * 16 + int(amm_addr[2:4]) % 10)
        buf.append(int(amm_addr[0:2]) / 10 * 16 + int(amm_addr[0:2]) % 10)
        buf.append(0x68)
        buf.append(0x01)
        buf.append(0x02)
        buf.append(cmd[0])
        buf.append(cmd[1])
        chsum = 0
        for chsumi in buf[2:14]:
            chsum = (chsum + chsumi) % 256
        buf.append(chsum)
        buf.append(0x16)
        # buflen = len(buf)
        return buf

    def create_write_buf(self,parame, cmd, vallen):
        buf = [0xFE]
        buf.append(0xFE)
        buf.append(0x68)
        buf.append(int(parame[6][10:12]) / 10 * 16 + int(parame[6][10:12]) % 10)
        buf.append(int(parame[6][8:10]) / 10 * 16 + int(parame[6][8:10]) % 10)
        buf.append(int(parame[6][6:8]) / 10 * 16 + int(parame[6][6:8]) % 10)
        buf.append(int(parame[6][4:6]) / 10 * 16 + int(parame[6][4:6]) % 10)
        buf.append(int(parame[6][2:4]) / 10 * 16 + int(parame[6][2:4]) % 10)
        buf.append(int(parame[6][0:2]) / 10 * 16 + int(parame[6][0:2]) % 10)
        buf.append(0x68)
        buf.append(0x04)
        buf.append(2 + 3 + vallen)
        buf.append(cmd[0])
        buf.append(cmd[1])
        buf.append(0x65 + 0x33 + 0x48 + 0x02)
        buf.append(0x43 + 0x33 + 0x54 + 0x03)
        buf.append(0x21 + 0x33 + 0x48 + 0x04)
        for valleni in range(0, vallen):
            if((valleni % 2) == 0):
                pass
        chsum = 0
        for chsumi in buf[2:17 + vallen]:
            chsum = (chsum + chsumi) % 256
        buf.append(chsum)
        buf.append(0x16)
        # buflen = len(buf)
        return buf
    def crackbuf(self,buf):
        returnbuf = []
        for bufj in range(0, len(buf)):
            if((bufj % 2) == 0):
                tempbuf = buf[bufj] - 0x33 - 0x48 - bufj
                if(tempbuf < 0):
                    tempbuf += 256            
                returnbuf.append(tempbuf)
            else:
                tempbuf = buf[bufj] - 0x33 - 0x54 - bufj
                if(tempbuf < 0):
                    tempbuf += 256
                returnbuf.append(tempbuf)
        return returnbuf       

    def contrl_byte(self,ser,amm_addr,cmd_value):
        cmd = [0x7f, 0xe4]  # contrl byte    
        value = [int(cmd_value)]
        ret = 0
        for i in xrange(12 - len(amm_addr)):
            amm_addr = '0' + amm_addr
        write_buf = self.create_wcmdwrite_buf(amm_addr, cmd, value)    
        try:
            if(value[0] > 15 or value[0] < 0):
                raise MyError.MyError('Error:value > 15 or value <0')      
            time.sleep(0.1)
            ser.write(write_buf)
            rece_data = []
            for k in range(12):
                intva = ord(ser.read(1))
                rece_data += [intva]
    # #        for i in rece_data:
    # #            print '%#x' % i,
            if(rece_data[8] == 0x84 and rece_data[9] == 0):
                ret = 0 #"write_contrl_byte Successful"
            else:
                raise MyError.MyError("write_contrl_byte Failed")
        except MyError.MyError as e:
            print e.value
            ret = e.value
        except:
            ret = "unknow error"
        return ret      
    def buy_electricity(self,ser, amm_addr, value):
        cmd = [0x8F, 0xE4]  # gou dian
        t_now = time.localtime()
        t = [t_now.tm_min, t_now.tm_hour, t_now.tm_mday, t_now.tm_mon, t_now.tm_year % 100, t_now.tm_year / 100]
        for i in xrange(len(t)):
            t[i] = (t[i] / 10) * 16 + t[i] % 10
    #        print '%#x' % t[i],
        if(len(amm_addr) != 12):
            for i in xrange(12 - len(amm_addr)):
                amm_addr = '0' + amm_addr
        try:
            goudf = value
            goudf = float(goudf)
            if(goudf <= 0 or goudf > 999999.99):
                raise MyError.MyError("Error:value <= 0 or value > 999999.99")
            gou_dian_liang = [int(goudf * 100 % 100), int(goudf % 100), int(goudf / 100 % 100), int(goudf / 10000 % 100)]
            for i in xrange(len(gou_dian_liang)):
                gou_dian_liang[i] = gou_dian_liang[i] / 10 * 16 + gou_dian_liang[i] % 10  
            send_data = self.create_read_buf(amm_addr, [0x0A, 0x6C])
            ser.write(send_data)
            rece_data = []
            for k in range(28):
                intva = ord(ser.read(1))
                rece_data += [intva]
            chsum = 0
            for chsumi in rece_data[0:26]:
                chsum = (chsum + chsumi) % 256
            if(chsum == rece_data[26]):                                                
                zuihycsd = self.crackbuf(rece_data[10:26])
    #            print zuihycsd[14:16]
                goudiancishu = int(((zuihycsd[15] / 16) * 10 + zuihycsd[15] % 16) * 100 + \
                                             ((zuihycsd[14] / 16) * 10 + zuihycsd[14] % 16))
                goudiancishu = (goudiancishu + 1) % 10000
                ci_shu = []
                ci_shu.append((goudiancishu % 100 / 10) * 16 + (goudiancishu % 100) % 10)
                ci_shu.append((goudiancishu / 100 / 10) * 16 + (goudiancishu / 100) % 10)
    
            value = [0x88, 0x88] + t + gou_dian_liang + ci_shu
    # #        for i in value:
    # #            print '%#x' % i,
    # #        print '\n'    
            write_buf = self.create_wcmdwrite_buf(amm_addr, cmd, value)
    # #        for i in write_buf:
    # #            print '%#x' % i,
    # #        print '\n'
            time.sleep(0.1)
            ser.write(write_buf)
            rece_data = []
            for k in range(12):
                intva = ord(ser.read(1))
                rece_data += [intva]
    # #        for i in rece_data:
    # #            print '%#x' % i,
            if(rece_data[8] == 0x84 and rece_data[9] == 0):
                time.sleep(0.1)
                goudiancishu_comp = 0
                send_data = self.create_read_buf(amm_addr, [0x0A, 0x6C])
                ser.write(send_data)
                rece_data = []
                for k in range(28):
                    intva = ord(ser.read(1))
                    rece_data += [intva]
                chsum = 0
                for chsumi in rece_data[0:26]:
                    chsum = (chsum + chsumi) % 256
                if(chsum == rece_data[26]):                                                
                    zuihycsd = self.crackbuf(rece_data[10:26])
        #            print zuihycsd[14:16]
                    goudiancishu_comp = int(((zuihycsd[15] / 16) * 10 + zuihycsd[15] % 16) * 100 + \
                                                 ((zuihycsd[14] / 16) * 10 + zuihycsd[14] % 16))
                if(goudiancishu_comp == goudiancishu): 
                    send_data = self.create_read_buf(amm_addr, [0x9A, 0x23])
                    ser.write(send_data)
                    rece_data = []
                    for k in range(26):
                        intva = ord(ser.read(1))
                        rece_data += [intva]
                    chsum = 0    
                    for chsumi in rece_data[0:24]:
                        chsum = (chsum + chsumi) % 256
                    if(chsum == rece_data[24]):                        
            # #                        print rece_data[10:24]
                        dqzydl = self.crackbuf(rece_data[10:24])
            #                             print ('dang qian zong yong dian liang + shengyu dian lian'+`dqzydl`)
                        sydl = dqzydl[6:10]
                        sydl.reverse()
                        sydl = float(((sydl[0] / 16) * 10 + sydl[0] % 16) * 10000 + \
                                     ((sydl[1] / 16) * 10 + sydl[1] % 16) * 100 + \
                                     ((sydl[2] / 16) * 10 + sydl[2] % 16) + \
                                     float((sydl[3] / 16) * 10 + sydl[3] % 16) / 100)
                    return {'_remaining_electricity':sydl}
                else:
                    raise MyError.MyError("Buy Electricity Failed")
            else:
                raise MyError.MyError("Buy Electricity Failed")
        except MyError.MyError as e:
            return e.value
        except:
            ser.close()    
            return "unknow error"
                
    def refund_electricity(self,ser, amm_addr, value):
        cmd = [0xAF, 0xE4]  # tui dian
        t_now = time.localtime()
        t = [t_now.tm_min, t_now.tm_hour, t_now.tm_mday, t_now.tm_mon, t_now.tm_year % 100, t_now.tm_year / 100]
        for i in xrange(len(t)):
            t[i] = (t[i] / 10) * 16 + t[i] % 10
    #        print '%#x' % t[i],
        if(len(amm_addr) != 12):
            for i in xrange(12 - len(amm_addr)):
                amm_addr = '0' + amm_addr
        try:
            goudf = value
            goudf = float(goudf)
            if(goudf <= 0 or goudf > 999999.99):
                raise MyError.MyError("Error:value <= 0 or value > 999999.99")
            gou_dian_liang = [int(goudf * 100 % 100), int(goudf % 100), int(goudf / 100 % 100), int(goudf / 10000 % 100)]
            for i in xrange(len(gou_dian_liang)):
                gou_dian_liang[i] = gou_dian_liang[i] / 10 * 16 + gou_dian_liang[i] % 10        
            send_data = self.create_read_buf(amm_addr, [0x2A, 0x6C])
            ser.write(send_data)
            rece_data = []
            for k in range(32):
                intva = ord(ser.read(1))
                rece_data += [intva]
            chsum = 0    
            for chsumi in rece_data[0:30]:
                chsum = (chsum + chsumi) % 256
            if(chsum == rece_data[30]):                                                
                zuihycsd = self.crackbuf(rece_data[10:26])
    # #            print zuihycsd[14:16]
                goudiancishu = int(((zuihycsd[15] / 16) * 10 + zuihycsd[15] % 16) * 100 + \
                                             ((zuihycsd[14] / 16) * 10 + zuihycsd[14] % 16))
                goudiancishu = (goudiancishu + 1) % 10000
                ci_shu = []
                ci_shu.append((goudiancishu % 100 / 10) * 16 + (goudiancishu % 100) % 10)
                ci_shu.append((goudiancishu / 100 / 10) * 16 + (goudiancishu / 100) % 10)   
            value = [0x88, 0x88] + t + gou_dian_liang + ci_shu
    # #        for i in value:
    # #            print '%#x' % i,
    # #        print '\n'    
            write_buf = self.create_wcmdwrite_buf(amm_addr, cmd, value)
    # #        for i in write_buf:
    # #            print '%#x' % i,
    # #        print '\n'
            time.sleep(0.1)
            ser.write(write_buf)
            rece_data = []
            for k in range(12):
                intva = ord(ser.read(1))
                rece_data += [intva]
    # #        for i in rece_data:
    # #            print '%#x' % i,
     
            if(rece_data[8] == 0x84 and rece_data[9] == 0):
                time.sleep(0.1)
                send_data = self.create_read_buf(amm_addr, [0x9A, 0x23])
                ser.write(send_data)
                rece_data = []
                for k in range(26):
                    intva = ord(ser.read(1))
                    rece_data += [intva]
                chsum = 0    
                for chsumi in rece_data[0:24]:
                    chsum = (chsum + chsumi) % 256
                if(chsum == rece_data[24]):                        
        # #                        print rece_data[10:24]
                    dqzydl = self.crackbuf(rece_data[10:24])
        #                             print ('dang qian zong yong dian liang + shengyu dian lian'+`dqzydl`)
                    sydl = dqzydl[6:10]
                    sydl.reverse()
                    sydl = float(((sydl[0] / 16) * 10 + sydl[0] % 16) * 10000 + \
                                 ((sydl[1] / 16) * 10 + sydl[1] % 16) * 100 + \
                                 ((sydl[2] / 16) * 10 + sydl[2] % 16) + \
                                 float((sydl[3] / 16) * 10 + sydl[3] % 16) / 100)
                return {'_remaining_electricity':sydl}
            else:
                raise MyError.MyError("RefundFailed")
        except MyError.MyError as e:
            return e.value
        except:
            return "unknow error"

    def write_metering(self,ser, amm_addr, value):
        cmd = [0x5F, 0xE4]  # jiliang
        if(len(amm_addr) != 12):
            for i in xrange(12 - len(amm_addr)):
                amm_addr = '0' + amm_addr
        try:
            if(not(value.isdigit()) and len(value) != 21):
                raise MyError.MyError("Error:value is not a number")
            temp = ''.join(value.split('.'))
    # #        print temp
            va = []
            for i in xrange(0, 12, 4):
                va.append(int(temp[i + 2:i + 4]))
                va.append(int(temp[i:i + 2]))
            for i in xrange(20, 12, -2):
                va.append(int(temp[i - 2:i]))
            for i in xrange(10):
                va[i] = va[i] / 10 * 16 + va[i] % 10
    # #            print '%#x' % va[i]
        
            write_buf = self.create_wcmdwrite_buf(amm_addr, cmd, va)
    # #        for i in write_buf:
    # #            print '%#x' % i,
    # #        print '\n'
            time.sleep(0.1)
            ser.write(write_buf)
            rece_data = []
            for k in range(12):
                intva = ord(ser.read(1))
                rece_data += [intva]
    # #        for i in rece_data:
    # #            print '%#x' % i,
            if(rece_data[8] == 0x84 and rece_data[9] == 0):
                return 0 #"Write_metering Successful"
            else:
                raise MyError.MyError("Write_metering Failed")
        except MyError.MyError as e:
            return e.value
        except:
            return "nuknow error"
    def create_wcmdwrite_buf(self,parame, cmd, value):
        buf = [0xFE]
        buf.append(0xFE)
        buf.append(0x68)
        buf.append(int(parame[10:12]) / 10 * 16 + int(parame[10:12]) % 10)
        buf.append(int(parame[8:10]) / 10 * 16 + int(parame[8:10]) % 10)
        buf.append(int(parame[6:8]) / 10 * 16 + int(parame[6:8]) % 10)
        buf.append(int(parame[4:6]) / 10 * 16 + int(parame[4:6]) % 10)
        buf.append(int(parame[2:4]) / 10 * 16 + int(parame[2:4]) % 10)
        buf.append(int(parame[0:2]) / 10 * 16 + int(parame[0:2]) % 10)
        buf.append(0x68)
        buf.append(0x04)
        buf.append(2 + len(value))
        temp = cmd[0] + 0x33 + 0x48
        if temp > 255:
            temp -= 256
        buf.append(temp)
        temp = cmd[1] + 0x33 + 0x54 + 1
        if temp > 255:
            temp -= 256
        buf.append(temp)
    # #    buf.append(0x65 + 0x33 + 0x48 + 0x02)
    # #    buf.append(0x43 + 0x33 + 0x54 + 0x03)
    # #    buf.append(0x21 + 0x33 + 0x48 + 0x04)
        for i in range(0, len(value)):
            if((i % 2) == 0):
                tempbuf = value[i] + 0x33 + 0x48 + i + 2
            else:
                tempbuf = value[i] + 0x33 + 0x54 + i + 2
            if(tempbuf > 255):
                tempbuf -= 256
            buf.append(tempbuf)               
        chsum = 0
        for chsumi in buf[2:14 + len(value)]:
            chsum = (chsum + chsumi) % 256
        buf.append(chsum)
        buf.append(0x16)
        # buflen = len(buf)
        return buf
    
#     def write_cmd_prepay_amm(self,param, index, var_name,var_value):
#         serport = self.get_serport_no()
#         for i in xrange(len(serport)):
#             if (serport[i] == param[0]):
#                 break
#         while(yj_global.SER_BESY[i] == True):
#             time.sleep(0.5)
#         ret = 'unknow error'
#         try:
#             if var_name == 'contrl_byte':
#                 ret = self.write_contrl_byte(param, param[index], str(var_value))
#             elif var_name == 'buy_electricity':
#                 ret = self.buy_electricity(param, param[index], str(var_value))
#             elif var_name == 'refund_electricity':
#                 ret = self.refund_electricity(param, param[index], str(var_value))
#             elif var_name == 'write_metering':
#                 ret = self.write_metering(param, param[index], str(var_value))
#             yj_global.SER_BESY[i] = False                            
#         except :
#             yj_global.SER_BESY[i] = False 
#             ret = 'unknow error 
#         return ret 
if __name__ == '__main__':
    mydev = Dtzy422()
    amm_addr = '814234'
#     print mydev.write_dev_value(PARAME[0],20,'contrl_byte',15)
#     print mydev.write_dev_value(PARAME[0],20,'buy_electricity',1.8)   #    value = 0-999999.99
#     print mydev.write_dev_value(PARAME[0],20,'refund_electricity',1)   #    value = 0-999999.99
    print mydev.read_dev_value(amm_addr,"/dev/ttyUSB0")   
# #    read_prepay_smm_sd('814234',p_amm_va_list,ser)
# #    ser.close()
#     sh = logging.StreamHandler()
#     sh.setLevel(10)
#     _log.addHandler(sh)
    


    
    
