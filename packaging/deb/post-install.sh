#!/usr/bin/env bash

NAME=tg120
BIN_DIR=/usr/bin
SCRIPT_DIR=./ #/usr/lib/$NAME/scripts
LOG_DIR=/var/log/$NAME
DAEMON_USER=$NAME
DAEMON_GROUP=$NAME
LOGROTATE=/etc/logrotate.d 

function install_init {
	cp -f $SCRIPT_DIR/init.sh /etc/init.d/$NAME
	chmod +x /etc/init.d/$NAME
	update-rc.d $NAME defaults
}

function install_systemd {
	cp -f $SCRIPT_DIR/$NAME.service /lib/systemd/system/$NAME.service
	systemctl daemon-reload
	systemctl enable $NAME

}

function restart_myserver {
	echo "Restarting Iot GateWay"
	which systemctl &>/dev/null
	if [[ $? -eq 0 ]]; then
		systemctl daemon-reload
		systemctl restart $NAME
	else
		/etc/init.d/ $NAME restart || true
	fi	
}

# create $NAME user
id  $NAME &>/dev/null
if [[ $? -ne 0 ]]; then
	useradd --system -U -M $NAME -d /bin/false
    usermod -aG dialout $NAME
fi

mkdir -p "$LOG_DIR"
chown  $NAME:$NAME"$LOG_DIR"
cp logrotate $LOGROTATE/$NAME 

if [[ -f /home/fa/tg120/$NAME.py ]] && [[ ! -e /usr/bin/$NAME ]]; then
    ln -s /home/fa/tg120/$NAME.py usr/bin/$NAME
fi
# add defaults file, if it doesn't exist
if [[ ! -f /etc/default/$NAME ]]; then
	cp $SCRIPT_DIR/default /etc/default/$NAME
	chown :$NAME /etc/default/$NAME
	chmod 640 /etc/default/$NAME
	echo -e "\n\n\n"
	echo "---------------------------------------------------------------------------------"
	echo "A sample configuration file has been copied to: /etc/default/$NAME"
	echo "After setting the correct values, run the following command to start $NAME server:"
	echo ""
	which systemctl &>/dev/null
	if [[ $? -eq 0 ]]; then
		echo "$ sudo systemctl start $NAME"
	else
		echo "$ sudo /etc/init.d/$NAME start"
	fi
	echo "---------------------------------------------------------------------------------"
	echo -e "\n\n\n"
fi

# add start script
which systemctl &>/dev/null
if [[ $? -eq 0 ]]; then
	install_systemd
else
	install_init
fi

if [[ -n $2 ]]; then
	restart_myserver
fi

