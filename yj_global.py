#!/usr/bin/python
# -*- coding: utf_8 -*-
# encoding: UTF-8
import asyncore
import logging
import os, sys, json
import signal
import time
import traceback
from singleton import SingletonLogging
from time import time as _time
from heapq import heapify, heappush, heappop

# some debugging
_debug = 0
_log = logging.getLogger(__name__)

# globals
_task_manager = None
_unscheduled_tasks = []
running = 1
taskManager = None
deferredFns = []
sleeptime = 0.0
CONFIGPATH = sys.path[0] + '/config.ini'


try:
    if not os.path.exists(CONFIGPATH) or os.path.getsize(CONFIGPATH) < 200L: 
        import shutil
        shutil.copyfile(sys.path[0] + '/connect_info.back',CONFIGPATH)
except:
    pass

SPIN = 1.0
def yj_debugging(obj):
    """Function for attaching a debugging logger to a class or function."""
    # create a logger for this object
    import logging.handlers
    logger = logging.getLogger(obj.__module__ + '.' + obj.__name__)
#    fh = logging.handlers.RotatingFileHandler(os.path.join(sys.path[0] , obj.__name__+'.log'),  maxBytes=1024*1024*50, backupCount=3, encoding='utf-8')
    ch = logging.StreamHandler()
    formats = logging.Formatter('%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s')
#    fh.setFormatter(formats)
    ch.setFormatter(formats)
    logger.setLevel(30)
#    logger.addHandler(fh)
    logger.addHandler(ch)
#     filter=logging.Filter(”abc.xxx”)
    # make it available to instances
    obj._logger = logger
    obj._info = logger.info
    obj._debug = logger.debug
    obj._warning = logger.warning
    obj._error = logger.error
    obj._exception = logger.exception
    obj._fatal = logger.fatal
    return obj  # }}}

#from warnings import warn
##元类需要继承type类
#class ReqStrSugRepr(type):
#  def __init__(cls, name, bases, attrd):
#  #构造函数需要传递的参数为类名，基类，类属性字典
#    super(ReqStrSugRepr, cls).__init__(name, bases, attrd)
#    # 判断__str__字符串是否在类的属性字典里
#    if '__str__' not in attrd:
#      raise TypeError('Class requires overriding of __str__()')
# 
#    if '__repr__' not in attrd:
#      warn('Class suggests overriding of __repr__()\n', stacklevel=3)
# 
#class Foo(object):
#  #给类指定元类 
#  __metaclass__ = ReqStrSugRepr
# 
#  def foo(self):
#    pass
##这一段代码不用创建类来测试，直接运行一下就会报。

#class _LoggingMetaclass(type):
#    def __init__(cls, *args):
#        # wrap the class
#        yj_debugging(cls)
#        
#class Logging(object):
#    __metaclass__ = _LoggingMetaclass
#
# only defined for linux platforms

if 'linux' in sys.platform:
    from event import WaitableEvent
    #
    #   _Trigger
    #
    #   An instance of this class is used in the task manager to break 
    #   the asyncore.loop() call.  In this case, handle_read will 
    #   immediately "clear" the event.
    #

    class _Trigger(WaitableEvent):

        def handle_read(self):
            if _debug: _Trigger._debug("handle_read")

            # read in the character, highlander
            data = self.recv(1)
            if _debug: _Trigger._debug("    - data: %r", data)# }}}

#
#   _Task
#

class _Task(object):

    _debug_contents = ('taskTime', 'isScheduled')
    
    def __init__(self):
        self.taskTime = None
        self.isScheduled = False

    def install_task(self, when=None):
        global _task_manager, _unscheduled_tasks

        # fallback to the inited value
        if when is None:
            when = self.taskTime
        if when is None:
            raise RuntimeError, "schedule missing, use zero for 'now'"
        self.taskTime = when
        
        # pass along to the task manager
        if not _task_manager:
            _unscheduled_tasks.append(self)
        else:
            _task_manager.install_task(self)

    def process_task(self):
        raise RuntimeError, "process_task must be overridden"

    def suspend_task(self):
        global _task_manager

        _task_manager.suspend_task(self)

    def resume_task(self):
        global _task_manager

        _task_manager.resume_task(self)# }}}

class RecurringTask(_Task):

    _debug_contents = ('taskInterval',)
    
    def __init__(self, interval=None):
        _Task.__init__(self)
        self.taskInterval = interval
        if interval is None:
            self.taskTime = None
        else:
            self.taskTime = _time() + (self.taskInterval / 1000)

    def install_task(self, interval=None):
        global _task_manager, _unscheduled_tasks
        
        # set the interval if it hasn't already been set
        if interval is not None:
            self.taskInterval = interval
        if not self.taskInterval:
            raise RuntimeError, "interval unset, use ctor or install_task parameter"
            
        # get ready for the next interval (aligned)
        now = _time()
        interval = self.taskInterval / 1000.0
        self.taskTime = now + interval - (now % interval)
        
        # pass along to the task manager
        if not _task_manager:
            _unscheduled_tasks.append(self)
        else:
            _task_manager.install_task(self)# }}}

class OneShotDeleteTask(_Task):

  def __init__(self, when=None):
      _Task.__init__(self)
      self.taskTime = when      # }}}

class TaskManager(SingletonLogging):

    def __init__(self):
        if _debug: TaskManager._debug("__init__")
        global _task_manager, _unscheduled_tasks

        # initialize
        self.tasks = []
        if 'linux' in sys.platform:
            self.trigger = _Trigger()
        else:
            self.trigger = None
            
        # task manager is this instance
        _task_manager = self

        # there may be tasks created that couldn't be scheduled
        # because a task manager wasn't created yet.
        if _unscheduled_tasks:
            for task in _unscheduled_tasks:
                self.install_task(task)
        
    def install_task(self, task):
        if _debug: TaskManager._debug("install_task %r %r", task, task.taskTime)

        # if this is already installed, suspend it
        if task.isScheduled:
            self.suspend_task(task)

        # save this in the task list
        heappush( self.tasks, (task.taskTime, task) )
        if _debug: TaskManager._debug("    - tasks: %r", self.tasks)

        task.isScheduled = True

        # trigger the event
        if self.trigger:
            self.trigger.set()

    def suspend_task(self, task):
        if _debug: TaskManager._debug("suspend_task %r", task)

        # remove this guy
        for i, (when, curtask) in enumerate(self.tasks):
            if task is curtask:
                if _debug: TaskManager._debug("    - task found")
                del self.tasks[i]

                task.isScheduled = False
                heapify(self.tasks)
                break
        else:
            if _debug: TaskManager._debug("    - task not found")

        # trigger the event
        if self.trigger:
            self.trigger.set()
        
    def resume_task(self, task):
        if _debug: TaskManager._debug("resume_task %r", task)
            
        # just re-install it
        self.install_task(task)

    def get_next_task(self):
        """get the next task if there's one that should be processed, 
        and return how long it will be until the next one should be 
        processed."""
        if _debug: TaskManager._debug("get_next_task")

        # get the time
        now = _time()

        task = None
        delta = None

        if self.tasks:
            # look at the first task
            when, nxttask = self.tasks[0]
            if when <= now:
                # pull it off the list and mark that it's no longer scheduled
                heappop(self.tasks)
                task = nxttask
                task.isScheduled = False

                if self.tasks:
                    when, nxttask = self.tasks[0]
                    # peek at the next task, return how long to wait
                    delta = max(when - now, 0.0)
            else:
                delta = when - now

        # return the task to run and how long to wait for the next one
        return (task, delta)

    def process_task(self, task):
        if _debug: TaskManager._debug("process_task %r", task)

        # process the task
        task.process_task()

        # see if it should be rescheduled
        if isinstance(task, RecurringTask):
            task.install_task()
        elif isinstance(task, OneShotDeleteTask):
            del task#}}}

def run(yjapp,spin=SPIN):
    _log.debug("run spin=%r", spin)
    global running, taskManager, deferredFns, sleeptime
    # reference the task manager (a singleton)
    taskManager = TaskManager()

    # count how many times we are going through the loop
    loopCount = 0

    rc = 1
    running = 0
    while running == 0:
#       _log.debug("time: %r", time.time())
        loopCount += 1
        # get the next task
        task, delta = taskManager.get_next_task()
        if(rc != 0):
            yjapp.write_parser(CONFIGPATH,"Mqtt","status","offline")
            try:
                yjapp.connect() 
            except:
                pass
            if yjapp.loop() == 0:
                online = json.dumps((yjapp.make_pub_update_msg('1', 'push/state.do')),ensure_ascii=False,indent=1,encoding='gbk')   
                deferred(yjapp.publish,online,True) 
                deferred(yjapp.subscribe)  
                yjapp.write_parser(CONFIGPATH,"Mqtt","status","online")
            time.sleep(1)
#             handle_config.set_on_offline(CONFIGPATH,'offline')
        try:
            rc = yjapp.loop()
            # if there is a task to process, do it
            if task:
                # _log.debug("task: %r", task)
                taskManager.process_task(task)

            # if delta is None, there are no tasks, default to spinning
            if delta is None:
                delta = spin

            # there may be threads around, sleep for a bit
            if sleeptime and (delta > sleeptime):
                time.sleep(sleeptime)
                delta -= sleeptime

            # if there are deferred functions, use a small delta
            if deferredFns:
                delta = min(delta, 0.001)
#           _log.debug("delta: %r", delta)

            # loop for socket activity
            #asyncore.loop(timeout=delta, count=1)

            # check for deferred functions
            while deferredFns:
                # get a reference to the list
                fnlist = deferredFns
                deferredFns = []
                
                # call the functions
                for fn, args, kwargs in fnlist:
                    # _log.debug("call: %r %r %r", fn, args, kwargs)
                    fn( *args, **kwargs)
                
                # done with this list
                del fnlist
                
        except KeyboardInterrupt:
            _log.info("keyboard interrupt")
            running = 1
        except Exception as e:
            _log.exception("an error has occurred: %s", e)
            
    running = 1

#
#   run_once
#

def run_once():   
    """
    Make a pass through the scheduled tasks and deferred functions just
    like the run() function but without the asyncore call (so there is no 
    socket IO actviity) and the timers.
    """
    _log.debug("run_once")
    global taskManager, deferredFns

    # reference the task manager (a singleton)
    taskManager = TaskManager()

    try:
        delta = 0.0
        while delta == 0.0:
            # get the next task
            task, delta = taskManager.get_next_task()
            _log.debug("    - task, delta: %r, %r", task, delta)

            # if there is a task to process, do it
            if task:
                taskManager.process_task(task)

            # check for deferred functions
            while deferredFns:
                # get a reference to the list
                fnlist = deferredFns
                deferredFns = []

                # call the functions
                for fn, args, kwargs in fnlist:
                    _log.debug("    - call: %r %r %r", fn, args, kwargs)
                    fn( *args, **kwargs)

                # done with this list
                del fnlist

    except KeyboardInterrupt:
        _log.info("keyboard interrupt")
    except Exception as e:
        _log.exception("an error has occurred: %s", e)       #}}}

#
#   stop
#

def stop(*args):
    """Call to stop running, may be called with a signum and frame 
    parameter if called as a signal handler."""
    _log.debug("stop")
    global running, taskManager

    if args:
        sys.stderr.write("===== TERM Signal, %s\n" % time.strftime("%d-%b-%Y %H:%M:%S"))
        sys.stderr.flush()

    running = False

    # trigger the task manager event
    if taskManager and taskManager.trigger:
        taskManager.trigger.set()

# set a TERM signal handler
if hasattr(signal, 'SIGTERM'):
    signal.signal(signal.SIGTERM, stop)

#
#   print_stack
#

def print_stack(sig, frame):
    """Signal handler to print a stack trace and some interesting values."""
    _log.debug("print_stack, %r, %r", sig, frame)
    global running, deferredFns, sleeptime

    sys.stderr.write("==== USR1 Signal, %s\n" % time.strftime("%d-%b-%Y %H:%M:%S"))

    sys.stderr.write("---------- globals\n")
    sys.stderr.write("    running: %r\n" % (running,))
    sys.stderr.write("    deferredFns: %r\n" % (deferredFns,))
    sys.stderr.write("    sleeptime: %r\n" % (sleeptime,))

    sys.stderr.write("---------- stack\n")
    traceback.print_stack(frame)

    # make a list of interesting frames
    flist = []
    f = frame
    while f.f_back:
        flist.append(f)
        f = f.f_back

    # reverse the list so it is in the same order as print_stack
    flist.reverse()
    for f in flist:
        sys.stderr.write("---------- frame: %s\n" % (f,))
        for k, v in f.f_locals.items():
            sys.stderr.write("    %s: %r\n" % (k, v))

    sys.stderr.flush()

# set a USR1 signal handler to print a stack trace
if hasattr(signal, 'SIGUSR1'):
    signal.signal(signal.SIGUSR1, print_stack)

#
#   deferred
#

def deferred(fn, *args, **kwargs):
    # _log.debug("deferred %r %r %r", fn, args, kwargs)
    global deferredFn

    # append it to the list
    deferredFns.append((fn, args, kwargs))

#
#   enable_sleeping
#

def enable_sleeping(stime=0.001):
    _log.debug("enable_sleeping %r", stime)
    global sleeptime

    # set the sleep time
    sleeptime = stime
    
  
            
