#!/usr/bin/python
# encoding: UTF-8
import time,sys,re,os
sys.path.append('..')
import logging
from serial.tools import list_ports as spl
class DynApp(object):
    devtypes = {}
    serstat = {}
    for pt in spl.comports():
       serstat[pt[0]] = 0
    @staticmethod
    def registerdev(app,devname):
        def call(cls):
            thiscls = cls()
            app.devtypes[devname] = thiscls
 #           setattr(thiscls,'ser',serial.Serial())
 #           app.devtypes[devname].ser.baudrate = thiscls.baudrate 
 #           app.devtypes[devname].ser.bytesize = thiscls.bytesize 
 #           app.devtypes[devname].ser.parity = thiscls.parity  
 #           app.devtypes[devname].ser.stopbits = thiscls.stopbits  
 #           app.devtypes[devname].ser.timeout = thiscls.timeout 
            return cls
        return call

    def load_drive(self,path):
        for pyfl in os.listdir(path):
            try:
                tstr = pyfl.split('.')
                if len(tstr) >1 and tstr[1] == r'py':
                    if re.search(r'(\w+).py$',pyfl) and pyfl.split('.')[0] != os.path.basename(__file__).split('.')[0]:
                        exec("from " +  re.search(r'(\w+).py$',pyfl).groups()[0] + " import *")
            except Exception:
                print sys.exc_info()[1]

class DevObj(object):
    def _getser(self,port,rw): 
        import serial
        ser = serial.Serial()
        ser.port = port
        ser.baudrate = self.baudrate       
        ser.bytesize = self.bytesize
        ser.parity = self.parity 
        ser.stopbits = self.stopbits
        ser.timeout = self.timeout 

        try:
            ser.open()
            DynApp.serstat[port] = rw
            #print "%s open sucess"%port
        except:
            DynApp.serstat[port] = 0
            print "%s open failed"%port
            return 0
        return ser
    
    def read_dev_value(self,addr,port): 
        ser = 0
        try:
            count = 0
            if not  DynApp.serstat.has_key(port):
                DynApp.serstat[port] = 0
            while DynApp.serstat[port] != 0:
                time.sleep(0.2)
                count += 1
                if count > 25:
                    raise RuntimeError,'timeout'
                    return "open serial timeout"
            ser = self._getser(port,1)
            if ser == 0:
                raise "open serial failed"
            DynApp.serstat[port] =1
            value = self.read_device(addr,ser)
            if len(value) == 0:
                raise "value is empty"
        except Exception,e:
            value = {'_status':'offline'}    
            time.sleep(0.2)
            self._error(e.message)
        if ser != 0 and ser.isOpen():
            ser.close()
        DynApp.serstat[port] = 0 
        return value
###################### user define method  #######################  
    def read_device(self,addr,ser):
        value = {'example_value':8888}
        return value
###################### user define method  ####################### 
    def write_dev_value(self,addr,port,var_name,var_value):
        ser = 0
        ret = 'unknow error'
        try:
            count = 0
            while DynApp.serstat[port] != 0:
                time.sleep(0.1)
                count += 1
                if count > 50:
                    raise RuntimeError,'timeout'
                    return "open serial timeout"
            ser = self._getser(port,2)
            if ser == 0:
                raise "open serial failed"
            else:
                DynApp.serstat[port] =2
                func = getattr(self,var_name,None)
                if func:
                    ret = func(ser, addr, var_value)
                else:
                    raise ValueError,'wrong data:%s'%var_name
        except Exception,e : 
            ret = e.message
        if ser != 0 and ser.isOpen():
            ser.close()
        DynApp.serstat[port] = 0
        return ret 
    
###################### user define method  #######################    
    def write_dev_cmd(self,ser,addr,cmd_value):
        ret = 0
        return ret
###################### user define method  ####################### 
    
   
def ModuleLogger(globs):
    # make sure that _debug is defined
    if not globs.has_key('_debug'):
        raise RuntimeError("define _debug before creating a module logger")
    # create a logger to be assigned to _log
    logger = logging.getLogger(globs['__name__'])
    logger.addHandler(logging.StreamHandler())
    # put in a reference to the module globals
    logger.globs = globs
    return logger

