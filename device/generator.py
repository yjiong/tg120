#!/usr/bin/python
# encoding:utf_8
import os,re
# for dnum in xrange(1,100):
#     if not os.path.exists(os.path.dirname(os.path.abspath("__file__")) + r'\Device_'+str(dnum) + r'.py'):
#         break
def mknewdev():
    print u'请输入你的设备类定义名'
    dname = raw_input()

    text = r'''#!/usr/bin/python
#encoding:UTF-8
import sys
from base import DynApp,DevObj 
from yj_global import yj_debugging 
from MyError import *
sys.path.append('..')

_debug = 0
#_log = base.ModuleLogger(globals())

################################---user code---################################
@DynApp.registerdev(DynApp,'设备名称型号')
################################---user code---################################
@yj_debugging
class DeviceName(DevObj):
    def __init__(self):
        if _debug: DeviceName._logger.setLevel(10);DeviceName._debug("__init__ ")
        self.set_config()
################################---user code---################################
#        定义自己的属性等
#        self.var1 = xxx
#        self.var2 = xxx
#        ......
################################---user code---################################
    @classmethod
    def set_config(cls):
       cls.baudrate = 9600 
       cls.bytesize = 8
       cls.parity = 'N'
       cls.stopbits = 1
       cls.timeout = 1  
        
################################---user code---################################
    def read_device(self,addr,ser):
        if _debug: DeviceName._debug(r"read_device   addr:%-10rport:%r"%(addr,ser.port))
        device_value = {}
#   参数addr为设备通信地址
#   参数ser是串口模块，方法：ser.write(data)date为字符串      ser.read(1)从串口读取一个字符
#   读取到所需要的数据后，将数据以字典的形式，赋值到device_value
#   device_value = {'var_name':xxxx,'var_name_1':xxxx,......}
        if _debug: DeviceName._debug(r'the device data is %r'%device_value)
        return device_value 
################################---user code---################################

################################---user code---################################ 
    def write_val(self,ser,addr,var_value):
        if _debug: DeviceName._debug(r"write_val     port:%-10raddr:%-10rvalue:%r"%(ser.port,addr,var_value))
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
#   对设备写命令方法，函数名按如下约定:
#   设备模板        "cmd": "do/setvar",
#             "data": {
#             "_varname": "write_val",
#             "_varvalue": "1.5"}
#    那么函数就定义为write_val(self,ser,addr,var_value)，设备模板里有多少个命令_varname，
#    就定义多少个方法，这些方法在接受到命令的时候自动被调用。
#    参数ser：串口模块，addr：设备通信地址，var_value：命令所需要的写入值
        if _debug: DeviceName._debug(r'the return code is %r'%ret)
        return ret
################################---user code---################################

   
###################### user define method  ####################################    
#   其他方法除了命名上不要和上面提到的重名。推荐使用私有方法即可
#
###################### user define method  ####################################  
   
if __name__ == '__main__':
    _debug = 1
    mydev = DeviceName()
    myser = mydev._getser("/dev/ttyUSB0",0)
    addr = 123456
    mydev.read_device(addr,myser) 
     
    value = 123
    mydev.write_val(myser,addr,value)   '''
    cc = re.sub(r'DeviceName',dname,text) 
    dirstr =  os.path.join(os.path.dirname(os.path.abspath(__file__)) , dname)
    with open(dirstr + r'.py','w') as f:
        f.write(cc)  
    print u'已生成驱动模板:%s'%dirstr
if __name__ == "__main__":
    mknewdev()
