#!/usr/bin/python
# encoding: UTF-8
import base
from base import DynApp,DevObj 
from yj_global import yj_debugging 
from MyError import *
import time 
_debug = 0
#_log = base.ModuleLogger(globals())

def h2bcd(hexv):
    if isinstance(hexv, list):
        retval = 0
        for h in hexv:
            if isinstance(h, int):
                retval *= 100
                retval += (h >> 4) * 10 + (h & 0xf)
            else:
                return 0
        return retval
    if isinstance(hexv, int):
        return (hexv >> 4) * 10 + hexv & 0xf
    return 0   
    
def chsum(cl):
    chsum = 0
    for chsumi in cl:
        chsum = (chsum + chsumi) % 256
    return chsum

@DynApp.registerdev(DynApp,'DDZY422-N')
@yj_debugging
class Ddzy422_n(DevObj):
    def __init__(self):
        if _debug: Ddzy422_n._debug("__init__ ")
        self.set_config()

    @classmethod
    def set_config(cls):
        cls.baudrate = 1200 
        cls.bytesize = 8
        cls.parity = 'E'
        cls.stopbits = 1
        cls.timeout = 1    

    def crackbuf(self,buf):
        returnbuf = []
        for bufj in range(0, len(buf)):
            if((bufj % 2) == 0):
                tempbuf = buf[bufj] - 0x33 - 0x48 - bufj
                if(tempbuf < 0):
                    tempbuf += 256            
                returnbuf.append(tempbuf)
            else:
                tempbuf = buf[bufj] - 0x33 - 0x54 - bufj
                if(tempbuf < 0):
                    tempbuf += 256
                returnbuf.append(tempbuf)
        return returnbuf        

    def create_read_buf(self,amm_addr, cmd):
#         if(len(amm_addr) != 12):
#             for i in xrange(12 - len(amm_addr)):
#                 amm_addr = '0' + amm_addr
        amm_addr = '%012d'%int(amm_addr)
        buf = [0xFE]
        buf.append(0xFE)
        buf.append(0x68)
        buf.append(int(amm_addr[10:12]) / 10 * 16 + int(amm_addr[10:12]) % 10)
        buf.append(int(amm_addr[8:10]) / 10 * 16 + int(amm_addr[8:10]) % 10)
        buf.append(int(amm_addr[6:8]) / 10 * 16 + int(amm_addr[6:8]) % 10)
        buf.append(int(amm_addr[4:6]) / 10 * 16 + int(amm_addr[4:6]) % 10)
        buf.append(int(amm_addr[2:4]) / 10 * 16 + int(amm_addr[2:4]) % 10)
        buf.append(int(amm_addr[0:2]) / 10 * 16 + int(amm_addr[0:2]) % 10)
        buf.append(0x68)
        buf.append(0x01)
        buf.append(0x02)
        buf.append(cmd[0])
        buf.append(cmd[1])
        chsum = 0
        for chsumi in buf[2:14]:
            chsum = (chsum + chsumi) % 256
        buf.append(chsum)
        buf.append(0x16)
        # buflen = len(buf)
        return buf
    
    def create_write_buf(self,amm_addr, cmd, value):
        amm_addr = '%012d'%int(amm_addr)
        buf = [0xFE]
        buf.append(0xFE)
        buf.append(0x68)
        buf.append(int(amm_addr[10:12]) / 10 * 16 + int(amm_addr[10:12]) % 10)
        buf.append(int(amm_addr[8:10]) / 10 * 16 + int(amm_addr[8:10]) % 10)
        buf.append(int(amm_addr[6:8]) / 10 * 16 + int(amm_addr[6:8]) % 10)
        buf.append(int(amm_addr[4:6]) / 10 * 16 + int(amm_addr[4:6]) % 10)
        buf.append(int(amm_addr[2:4]) / 10 * 16 + int(amm_addr[2:4]) % 10)
        buf.append(int(amm_addr[0:2]) / 10 * 16 + int(amm_addr[0:2]) % 10)
        buf.append(0x68)
        buf.append(0x04)
        buf.append(2 + 3 + len(value))
        buf.append(cmd[0])
        buf.append(cmd[1])
        buf.append(0x65 + 0x33 + 0x48 + 0x02)
        buf.append(0x43 + 0x33 + 0x54 + 0x03)
        buf.append(0x21 + 0x33 + 0x48 + 0x04)
        for i in range(0, len(value)):
            if((i % 2) == 0):
                tempbuf = value[i] + 0x33 + 0x54 + i + 5
            else:
                tempbuf = value[i] + 0x33 + 0x48 + i + 5
            if(tempbuf > 255):
                tempbuf -= 256
            buf.append(tempbuf)   
        chsum = 0
        for chsumi in buf[2:17 + len(value)]:
            chsum = (chsum + chsumi) % 256
        buf.append(chsum)
        buf.append(0x16)
        # buflen = len(buf)
        return buf   
    def read_device(self,addr,ser):
#         #serial inWaiting,readall()
#             剩余金额 4byte _remaining_sum
#             透支金额4byte  _overdraft
#             电量 4byte  _total_electricity_consumption
#             状态 1byte _status_byte
#             报警 1byte 
#             控制 1byte
        send_data = self.create_read_buf(addr, [0xBA, 0x78])
        ser.write(send_data)
        rece_data = []
        for k in xrange(29):
            intva = ord(ser.read(1))
            rece_data += [intva]
        index = rece_data.index(0x68)
        if rece_data[index + 28] != 0x16 or chsum(rece_data[index:index + 27]) != rece_data[index + 27]:
            raise MyError('wrong read')
        crack_data = self.crackbuf(rece_data[index + 10:index + 27])
#         print [hex(x) for x in crack_data]
        _remaining_sum =  h2bcd(crack_data[2:6][::-1]) / 100.0
        _overdraft =  h2bcd(crack_data[6:10][::-1]) / 100.0
        _total_electricity_consumption = h2bcd(crack_data[10:14][::-1]) / 100.0
        _status_byte = crack_data[14] #,crack_data[15],crack_data[16]
        if _status_byte & 0x40:
            _switch = "off"
        else:
            _switch = "on"
#         print "---------------------read data--------------------------------" 
#             报警金额2byte '_minimum_alarm':
#             报警负荷2byte '_overloaded_alarm'
#             允许透支金额2byte '_permit_overdraft'
#             允许囤积金额4byte '_permit_Hoarding'
#             金额报警跳闸时间2byte '_tripping_operation_time'
#             报警方式；01跳闸，02声光报警，03全 1byte
#             xx.xx为单价，单位为（元），NN为费率号(1byte)，无单位，用0为尖，1为峰，2为平，3为谷。(1+2bye)*4
        time.sleep(0.05)
        send_data = self.create_read_buf(addr, [0x6A, 0x78])
#         print [hex(x) for x in send_data]
        ser.write(send_data)
        rece_data = []
        for k in xrange(39):
            intva = ord(ser.read(1))
            rece_data += [intva]
        index = rece_data.index(0x68)
        if rece_data[index + 38] != 0x16 or chsum(rece_data[index:index + 37]) != rece_data[index + 37]:
            raise MyError('wrong read')
        crack_data = self.crackbuf(rece_data[index + 10:index + 37])
#         print [hex(x) for x in crack_data]
        _minimum_alarm = h2bcd(crack_data[2:4][::-1])
        _overloaded_alarm =  h2bcd(crack_data[4:6][::-1]) / 100.0
        _permit_overdraft = h2bcd(crack_data[6:8][::-1])
        _permit_Hoarding = h2bcd(crack_data[8:12][::-1]) / 100.0
        _tripping_operation_time = h2bcd(crack_data[12:14][::-1])
#             crack_data[14],\
        jfpg = ['_Sharp_price', '_peak_price','_flat_price', '_valley_price'] 
        if crack_data[15] >=4 or crack_data[18] >=4 or crack_data[21] >=4 or crack_data[24] >=4:
            jfpg = ['','_Sharp_price', '_peak_price','_flat_price', '_valley_price'] 
        Sharp = (jfpg[crack_data[15]],h2bcd(crack_data[16:18][::-1]) / 100.0)
        peak = (jfpg[crack_data[18]],h2bcd(crack_data[19:21][::-1]) / 100.0)
        flat = (jfpg[crack_data[21]],h2bcd(crack_data[22:24][::-1]) / 100.0)
        valley = (jfpg[crack_data[24]],h2bcd(crack_data[25:27][::-1]) / 100.0)
        device_value = {\
                        '_remaining_money':_remaining_sum,\
                        '_overdraft':_overdraft,\
                        '_total_electricity_consumption':_total_electricity_consumption,\
#                         '_status_byte':_status_byte,\
#                         '_switch':_switch,\
                        '_minimum_alarm':_minimum_alarm,\
                        '_overloaded_alarm':_overloaded_alarm,\
                        '_permit_overdraft':_permit_overdraft,\
                        '_permit_Hoarding':_permit_Hoarding,\
                        '_tripping_operation_time':_tripping_operation_time,\
                        Sharp[0]:Sharp[1],\
                        peak[0]:peak[1],\
                        flat[0]:flat[1],\
                        valley[0]:valley[1]\
                            }
        return device_value

    def write_init_data(self,ser,addr,var_value):#0x6A78
        ret = 0 
#         bsend = [0x20,0x0,0x20,0x11,0x8,0,0x99,0x99,0x99,0x99,0x60,0,3,0,0x88,0x01,1,0x11,0x1,2,0x55,0x0,3,0x33,0x0]
        if isinstance(var_value, dict):
            keys = ["_minimum_alarm", 
                  "_overloaded_alarm", 
                  "_permit_overdraft", 
                  "_permit_Hoarding",
                  "_tripping_operation_time", 
                  "_Sharp_price", 
                  "_peak_price", 
                  "_flat_price", 
                  "_valley_price"]
            for key in keys:
                if key not in var_value.keys():
                    ret = MyError('wrong data')
                    raise MyError('wrong data')
                    return ret
            send = []
            v1= '%04d'%var_value['_minimum_alarm'] 
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2]))
            v1= '%04d'%(var_value['_overloaded_alarm'] * 100)
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2]))
            v1= '%04d'%var_value['_permit_overdraft'] 
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2]))  
            v1= '%08d'%(var_value['_permit_Hoarding'] * 100)
            send += eval('[0x%s,0x%s,0x%s,0x%s]'%(v1[-8:][-2:],v1[-8:][-4:-2],v1[-8:][-6:-4],v1[-8:][-8:-6]))
            send += [0x60,0,3,0]
            v1= '%04d'%(var_value['_Sharp_price'] * 100)
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2]))
            send.append(1) 
            v1= '%04d'%(var_value['_peak_price'] * 100)
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2])) 
            send.append(2) 
            v1= '%04d'%(var_value['_flat_price'] * 100)
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2])) 
            send.append(3) 
            v1= '%04d'%(var_value['_valley_price'] * 100)
            send += eval('[0x%s,0x%s]'%(v1[-4:][-2:],v1[-4:][:2]))    
#             print [hex(x) for x in send]                      
        send_data = self.create_write_buf(addr,[0x6A,0x78],send)
        ser.write(send_data)
        rece_data = []
        for k in xrange(12):
            intva = ord(ser.read(1))
            rece_data += [intva]  
        index = rece_data.index(0x68)
        if rece_data[len(rece_data) - 1] != 0x16 or chsum(rece_data[index:len(rece_data) - index -2]) != rece_data[len(rece_data) - 2]:
            ret = MyError('wrong read')
            raise MyError('wrong read')       
#         print [hex(x) for x in rece_data]
        return ret        
        
    def write_charge_money(self,ser,addr,var_value,cmd = [0x0A,0x7C]):#0x0A, 0x7C
        ret = 0 
        var_value = float(var_value)
        if(var_value <= 0 or var_value > 9999.99):
            ret = MyError('wrong data')
            raise MyError("Error:value <= 0 or value > 9999.99")
            return ret
        send_data = self.create_read_buf(addr, cmd)
#         print [hex(x) for x in send_data]
        ser.write(send_data)
        rece_data = []
        for k in xrange(27):
            intva = ord(ser.read(1))
            rece_data += [intva]
#        rece_data = [ord(d) for d in ser.readall()]
#         print len(rece_data)
        index = rece_data.index(0x68)
        if rece_data[index + 26] != 0x16 or chsum(rece_data[index:index + 25]) != rece_data[index + 25]:
            ret = MyError('wrong read')
            raise MyError('wrong read')
         
        crack_data = self.crackbuf(rece_data[index + 10:index + 25])
        branch = crack_data[2:4]
        buycount = [(h2bcd(crack_data[13:][::-1]) + 1) % 100, ((h2bcd(crack_data[13:][::-1]) +1 )/ 100) % 100]
        buycount = eval('[0x%d,0x%d]'%tuple(buycount))
#         print [hex(x) for x in crack_data],'\n',branch,buycount
        import time
        t_now = time.localtime()
        l_t = [t_now.tm_min, t_now.tm_hour, t_now.tm_mday, t_now.tm_mon, t_now.tm_year % 100, t_now.tm_year / 100]
        s_t = [eval('0x%d'%x) for x in l_t]
#         print s_t
        m = '%06d'%(var_value * 100)
#         print m
        s_m = eval('[0x%s,0x%s,0x%s]'%(m[4:6],m[2:4],m[0:2]))
#         print [hex(x) for x in s_m]
        s_var = branch + s_t + s_m + buycount
#         print [hex(x) for x in s_var]
        send_data = self.create_write_buf(addr,cmd, s_var)
        ser.write(send_data)
#        rece_data = [ord(d) for d in ser.readall()]
        rece_data = []
        for k in xrange(12):
            intva = ord(ser.read(1))
            rece_data += [intva]     

        index = rece_data.index(0x68)
        if rece_data[len(rece_data) - 1] != 0x16 or chsum(rece_data[index:len(rece_data) - index -2]) != rece_data[len(rece_data) - 2]:
            ret = MyError('wrong read')
            raise MyError('wrong read')  
#reread buycount if =+1 ok        
        send_data = self.create_read_buf(addr, cmd)
#         print [hex(x) for x in send_data]
        ser.write(send_data)
        rece_data = []
        for k in xrange(27):
            intva = ord(ser.read(1))
            rece_data += [intva]            
#        rece_data = [ord(d) for d in ser.readall()]
#         print len(rece_data)
        index = rece_data.index(0x68)
        if rece_data[index + 26] != 0x16 or chsum(rece_data[index:index + 25]) != rece_data[index + 25]:
            ret = MyError('wrong read')
            raise MyError('wrong read')
        crack_data = self.crackbuf(rece_data[index + 10:index + 25]) 
        rebuycount = [h2bcd(crack_data[13:][::-1]) % 100, (h2bcd(crack_data[13:][::-1])/ 100) % 100]
        rebuycount = eval('[0x%d,0x%d]'%tuple(rebuycount))
        if rebuycount != buycount:
            ret = MyError('charge money failed')
            raise MyError('charge money failed')
        else:
            send_data = self.create_read_buf(addr, [0xBA, 0x78])
            ser.write(send_data)
            rece_data = []
            for k in xrange(29):
                intva = ord(ser.read(1))
                rece_data += [intva]
            index = rece_data.index(0x68)
            if rece_data[index + 28] != 0x16 or chsum(rece_data[index:index + 27]) != rece_data[index + 27]:
                raise MyError('wrong read')
            crack_data = self.crackbuf(rece_data[index + 10:index + 27])
    #         print [hex(x) for x in crack_data]
            _remaining_sum =  h2bcd(crack_data[2:6][::-1]) / 100.0
            ret = {'_remaining_money':_remaining_sum}
#         print [hex(x) for x in rece_data]   
        return ret

    def write_refund_money(self,ser,addr,var_value):#0x2A,0x7C
        return self.write_charge_money(ser, addr, var_value, cmd = [0x2A,0x7C])
    
    def operat_switch(self,ser,addr,var_value):
        ret = 0;cmd = 0
        if isinstance(var_value, dict):
            if var_value.keys()[0] == "_master_switch":
                cmdd = {'on':[0xDC,0x78],'off':[0xDD,0x78],'restore':[0xDE,0x78]}
                if var_value.values()[0] in cmdd.keys():
                    cmd = cmdd[var_value.values()[0]]
            if var_value.keys()[0] == "_assist_switch":
                cmdd = {'on':[0xDF,0x78],'off':[0xE0,0x78],'restore':[0xE1,0x78]}
                if var_value.values()[0] in cmdd.keys():
                    cmd = cmdd[var_value.values()[0]]
        if cmd != 0:
            send_data = self.create_write_buf(addr,cmd,[])
            ser.write(send_data)
            rece_data = []
            for k in xrange(12):
                intva = ord(ser.read(1))
                rece_data += [intva]  
            index = rece_data.index(0x68)
            print [hex(x) for x in rece_data]
            if rece_data[len(rece_data) - 1] != 0x16 or\
            chsum(rece_data[index:len(rece_data) - index -2]) != rece_data[len(rece_data) - 2] or\
            rece_data[len(rece_data) - 3] != 0:
                ret = MyError('wrong read')
                raise MyError('wrong read')  
        else:       
            ret = MyError('wrong data')
            raise MyError('wrong data')
            return ret
        return ret
    
        
if __name__ == '__main__':
    import sys,json
    mydev = Ddzy422_n()
    myser = mydev._getser("/dev/ttyUSB0",0)
#     ser = mydev._openser(mydev.set_config())    
    amm_addr = '836647'
    init_data={
 "_minimum_alarm": 10, 
 "_overloaded_alarm": 10.0, 
 "_permit_overdraft": 10, 
 "_permit_Hoarding": 999999,
 "_tripping_operation_time": 60, 
 "_Sharp_price": 0.00, 
 "_peak_price": 0.00, 
 "_flat_price": 0.00, 
 "_valley_price": 0.00
}
    try:
#         print json.dumps(mydev.write_init_data(myser,amm_addr,init_data),encoding='utf-8',ensure_ascii=False,indent=1)
#         print json.dumps(mydev.write_charge_money(myser,amm_addr,2),encoding='utf-8',ensure_ascii=False,indent=1)
        print json.dumps(mydev.write_refund_money(myser,amm_addr,2),encoding='utf-8',ensure_ascii=False,indent=1)
#         print json.dumps(mydev.operat_switch(myser,amm_addr,{'_master_switch':'on'}),encoding='utf-8',ensure_ascii=False,indent=1)
#         print json.dumps(mydev.read_device('822847',myser),encoding='utf-8',ensure_ascii=False,indent=1)
        print json.dumps(mydev.read_device(amm_addr,myser),encoding='utf-8',ensure_ascii=False,indent=1)
    except MyError as e:
        print e.value
# #    ser.close()
#     sh = logging.StreamHandler()
#     sh.setLevel(10)
#     _log.addHandler(sh)
    


    
    
