#!/bin/bash
#sdcard=(`ls /dev |grep -E "mmcblk[0-9]$|sd[a-z]$"`)
set -e
MYDIR=/media/mymedia/home/fa/tg150
CONFILE=config.ini
IDSTR="_client_id = TG150-"

sdcard=(`cat /proc/partitions |grep -E "mmcblk[0-9]$|sd[b-z]$"|awk '{print $4}'`)
sdsize=(`cat /proc/partitions |grep -E "mmcblk[0-9]$|sd[b-z]$"|awk '{print (($3/1024/1024))"G"}'`)
if [ $(id -u) -ne 0 ]; then
	echo "please running script under root"
	exit 1
fi
clone ()
{
    echo start write to $1 with client id $2
    mkdir -p /media/mymedia 
    mount -o loop rootfs.img /media/mymedia
    sedfile=$MYDIR/$CONFILE
    sed -i "s/\(${IDSTR}\).*/\1${2}/" ${sedfile}		
       sedid=$(sed -n "s/${IDSTR}\(.*\)/\1/p" ${sedfile})
    sleep 1
    fuser -m -k /media/mymedia
    umount /media/mymedia
    tempv=`ls /media/mymedia|wc -l`
       if [ ${tempv}x == 0x ];then
    rm -r /media/mymedia
    fi
    dd if=./boot.img of=/dev/${1} bs=1024 count=65536
	dd if=./rootfs.img of=/dev/${1} bs=1024 seek=65536
    echo "正在同步,请稍侯......"
    sync
	if [[ $1 =~ mmcblk[0-9] ]];then
		sdpart=${1}p2
	else
		sdpart=${1}2
	fi
    echo "请拔出sd卡,重新插入,回车继续......"
    read sdbxxxxx
    while true ;do
        sdb=`ls /dev/ |grep "${sdpart}"`
        if [  x${sdb} == x${sdpart} ];then
            echo -e "识别到${sdb}"
            break
        else
            echo "未识别到sd卡分区,请重新插拔,回车继续......"
            read sdbxxxxx
        fi
        #sleep 1
    done
	#partprobe /dev/${1} -s 2>/dev/null
	umount /dev/$sdpart
	e2fsck -fy /dev/$sdpart 
    e2fs=$?
	resize2fs -f /dev/$sdpart
    sync
    if [[ ${2} == ${sedid} ]] && [ ${e2fs} -lt 4 ];then 
		echo clone successful
	else
        echo clone disk faild ,check your disk !!!
       fi
}
sure=n
while [ x${sure} != xy ];do
	printf "input your device client_id \n"
	read client
	if [ ! -z $client ];then 	
		echo -e device id is "\033[47;30m${client}\033[0m",are you sure ? y/n
		read sure
	fi
done
printf '\n'
for(( i=0;i<${#sdcard[@]};i++ ))
do
#	echo $i:${sdcard[$i]} size=${sdsize[$i]}
	printf "%s:%-10ssize=%s \n" $i ${sdcard[$i]} ${sdsize[$i]}
done
printf '\n'
echo choice your target sd disk number
read choice
echo -e clone image to "\033[47;30m${sdcard[choice]}\033[0m" ,are you sure ? y/n
read sure
if [ x${sure} = xy ];then
	target=${sdcard[choice]}
	while (ps auxww |grep " dd " |grep -v grep |awk '{print $2}' |while read pid; do kill -USR1 $pid; done) ; do sleep 3; done &
	clone $target $client
	kill %1
elif [ x${sure} = xn ];then
	echo byebye !!!
	exit 0		
fi
	exit 0	

