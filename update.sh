#! /bin/bash

NAME=tg120
MYPATH=/home/fa
wgetpath=`echo $1 | grep -i -E ".*/(drive.tar.gz|${NAME}([1-9,-]|.){0,10}.tar.gz)"`
if [ ! $1 ];then
	echo wrong date
	exit 1
fi
if [ ! ${wgetpath} ];then
	echo wrong date
	exit 1
fi
function cpnewscript ()
{
    tar czf ${MYPATH}/bac.tgz -C $MYPATH/$NAME/ .
    if [[ $wgetpath =~ .*/drive.tar.gz ]];then
    	tar xzf /tmp/drive.tar.gz -C /$MYPATH/$NAME
    	systemctl restart ${NAME}.service || service restart $NAME
    	exit 0	
    elif [[ $wgetpath =~ .*/$NAME([1-9,-]|.){0,10}.tar.gz ]];then
    	tar xzf /tmp/$(basename $wgetpath) -C $MYPATH 
        systemctl restart ${NAME}.service  || service restart $NAME  
    	if [[ -z $(ps -aux | grep -E "/usr/bin/.*${NAME}$" | sed "/.*grep.*/d") ]];then
    	    tar xzf ${MYPATH}/bac.tgz -C $MYPATH/$NAME 
            systemctl restart ${NAME}.service || service restart $NAME
            echo update faild
            exit 1
        else
            echo update successful
            exit 0
        fi          
    fi
}
rm -f /tmp/$(basename $wgetpath)
#wget --spider $wgetpath 2> /$MYPATH/temp
wget -c $wgetpath -O /tmp/$(basename $wgetpath)
#size=$(du -b /tmp/$(basename $wgetpath) | awk '{print $1}')
if [[ $? -eq 0 ]];then
#if [ "$(sed -n 's/Length:\s\([0-9]\+\)\s.*/\1/p' /$MYPATH/temp)" = "$size" ];then
    cpnewscript
    rm -f /tmp/$(basename $wgetpath)
else
    exit 1
fi
