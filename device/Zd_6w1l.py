#!/usr/bin/python
#encoding:UTF-8
import base
from base import DynApp,DevObj 
from yj_global import yj_debugging 
import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
from MyError import *
_debug = 0
#_log = base.ModuleLogger(globals())

@DynApp.registerdev(DynApp,'ZD-6W1L')
@yj_debugging
class Zd_6w1l(DevObj):
    def __init__(self):
        if _debug: Zd_6w1l._logger.setLevel(10);Zd_6w1l._debug("__init__ ")
        self.set_config()
################################---user code---################################
#        定义自己的属性等
#        self.var1 = xxx
#        self.var2 = xxx
#        ......
################################---user code---################################
    @classmethod
    def set_config(cls):
        cls.baudrate = 9600 
        cls.bytesize = 8
        cls.parity = 'N'
        cls.stopbits = 1
        cls.timeout = 1 
   
        
################################---user code---################################
    def read_device(self,addr,ser):
        if _debug: Zd_6w1l._debug(r"read_device   addr:%-10rport:%r"%(addr,ser.port))
        device_value = {}
#   参数addr为设备通信地址
#   参数ser是串口模块，方法：ser.write(data)date为字符串      ser.read(1)从串口读取一个字符
#   读取到所需要的数据后，将数据以字典的形式，赋值到device_value
#   device_value = {'var_name':xxxx,'var_name_1':xxxx,......}
        master = modbus_rtu.RtuMaster(ser)
        master.set_timeout(1)
        try:
#             master.execute(1,cst.WRITE_MULTIPLE_REGISTERS,75,5,[0x000b,0x0102,0x0304,0x0506,0x0708])
            mydata = master.execute(int(addr), cst.READ_INPUT_REGISTERS,0,2)
#             print mydata[0]*0x10000+mydata[1]
            device_value.update({'_illumination':mydata[0]*0x10000+mydata[1]})
            
        except:
            print 'device is not online'
        if _debug: Zd_6w1l._debug(r'the device data is %r'%device_value)
        return device_value 
################################---user code---################################
                
################################---user code---################################ 
    def write_val(self,ser,addr,var_value):
        if _debug: Zd_6w1l._debug(r"write_val     port:%-10raddr:%-10rvalue:%r"%(ser.port,addr,var_value))
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
#   对设备写命令方法，函数名按如下约定:
#   设备模板        "cmd": "do/setvar",
#             "data": {
#             "_varname": "write_val",
#             "_varvalue": "1.5"}
#    那么函数就定义为write_val(self,ser,addr,var_value)，设备模板里有多少个命令_varname，
#    就定义多少个方法，这些方法在接受到命令的时候自动被调用。
#    参数ser：串口模块，addr：设备通信地址，var_value：命令所需要的写入值
        if _debug: Zd_6w1l._debug(r'the return code is %r'%ret)
        return ret
################################---user code---################################

   
###################### user define method  ####################################    
#   其他方法除了命名上不要和上面提到的重名。推荐使用私有方法即可
#
###################### user define method  ####################################  
   
if __name__ == '__main__':
    _debug = 0
    mydev = Zd_6w1l()
    myser = mydev._getser("/dev/ttyUSB0",0)
    addr = 1
    print   mydev.read_device(addr,myser) 
     
#     value = 123
#     mydev.write_val(myser,addr,value)   
