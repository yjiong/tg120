#!/usr/bin/python
# encoding:UTF-8
# from curses.ascii import isdigit
from ConfigParser import ConfigParser
from flask import Flask, render_template
from flask import request
import gevent
from gevent.monkey import patch_all
from gevent.pywsgi import WSGIServer
from gevent.queue import PriorityQueue
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler
import  sys, time, json, re, codecs , os ,gpio
import traceback  # ,collections

from device.base import DynApp
from  webpy.myweb import *
import init_set
from yj_global import run
import yj_global, inspect
# from werkzeug.contrib.cache import _items
patch_all()

# logging.basicConfig(level=logging.DEBUG)
# log = logging.getLogger(__name__)
try:
    import paho.mqtt.client as mqtt
except ImportError:
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], "../src")))
    if cmd_subfolder not in sys.path:
        sys.path.insert(0, cmd_subfolder)
# some debugging
_debug = 0
#_log = ModuleLogger(globals())


def trace():
    #     import traceback,inspect
    print inspect.stack()[0][3]
    s = traceback.extract_stack()
    print "%-80s%-10s%-30s%-60s" % ('path', 'line', 'funname', 'selffun')
    for ss in s:
        print '{0:<80}{1:<10}{2:<30}{3:<60}'.format(ss[0], ss[1], ss[2], ss[3])# }}}

@yj_global.yj_debugging
class YjApp(mqtt.Mosquitto, init_set.Argumentparser, Flask,DynApp):
    def __init__(self, configpath, import_name):# {{{
        self._modelname = 'TG120'
        self._version = 'v2.0'
        init_set.Argumentparser.__init__(self,  configpath)
        mqtt.Mosquitto.__init__(self, 'yjiong' + self.eviron['_client_id'])
        Flask.__init__(self, import_name, static_folder=sys.path[0] + '/static', template_folder=sys.path[0] + '/templates')
        self.readdevname()
        self.on_message = self.mqtt_message
        self.username_pw_set(self.eviron['_username'], self.eviron['_password'])
        self.willmsg = json.dumps(self.make_pub_update_msg('0', 'push/state.do'))
        self.will_set(self.eviron['_server_name'] + '/' + self.eviron['_client_id'], self.willmsg, 1, True)
        self.subTopic = (self.eviron['_client_id'] + '/' + self.eviron['_server_name']).encode("utf-8")
        self.pubTopic = (self.eviron['_server_name'] + '/' + self.eviron['_client_id']).encode("utf-8")
        self.pqueue = PriorityQueue()
        self.whoislist = {}
        self.cmd_msg = {}
        self.vardict = {}
        self.users = set()
        if self.eviron["capath"]: #and self.eviron["certfile"] and self.eviron["keyfile"]:
            self.tls_set(self.eviron["capath"])
            #self.tls_set(ca_certs = self.eviron["capath"],certfile = self.eviron["certfile"],keyfile = self.eviron["keyfile"])
        # keep track of requests to line up responses
#         self._request = None# }}}
        self.GPIO_RESET = 65
        self.GPIO_RUN = 67
        self.GPIO_LINK = 66
        try:
            gpio.setup(self.GPIO_RESET,"in")
            gpio.setup(self.GPIO_RUN,"out")
            gpio.setup(self.GPIO_LINK,"out")
            self.gpio = True
        except:
            self.gpio = False
        if self.gpio:
            gevent.spawn(self.resetdefip).start
            gevent.spawn(self.led_run,self.GPIO_RUN,0.5).start
            gevent.spawn(self.led_link,self.GPIO_LINK,1).start

    def readdevname(self):# {{{
        if not os.path.exists(sys.path[0] + '/devname.ini'):
            with open(sys.path[0] + '/devname.ini', 'w') as f:
                conf = ConfigParser()
                conf.add_section('devlist')
                conf.write(f)
                self.devname = dict(conf.items('devlist'))
        else:
            conf = ConfigParser()
            with codecs.open(sys.path[0] + '/devname.ini', encoding='utf-8') as f:
                conf.readfp(f)
    #             ini_obj = type('devname', (), dict(conf.items('devlist')))
                setattr(self, 'devname', dict(conf.items('devlist')))# }}}

    def md2str(self,data):
        if isinstance(data,str):
            return data
        if isinstance(data,dict):
            portlist = self.eviron['serial'].split(',')
            if data.has_key('_conn') and data.has_key('_type') and data.has_key('_devid'):
                if data['_conn'].has_key('port') and data['_conn'].has_key('devaddr'):
                    joinlist = [data['_type'],portlist[int(data['_conn']['port'])],data['_conn']['devaddr']]
                    elementstr = ','.join(joinlist)
                    return elementstr
        raise RuntimeError,u'格式错误--%r'%data

    def writedevname(self, opt, value):# {{{
        val = self.md2str(value)
        conf = ConfigParser()
        conf.read(sys.path[0] + '/devname.ini')
        if val not in self.devname.values():
            conf.set('devlist', opt, val)
            conf.write(open(sys.path[0] + '/devname.ini', 'w'))
            self.readdevname()
            self.set_runstate()
            return u"(_devid:%s)更新成功" % unicode(opt, 'utf-8')
        else:
            raise RuntimeError, u'设备已经存在--%r'%value  # }}}

    def deldevname(self, opt):# {{{
        conf = ConfigParser()
        conf.read(sys.path[0] + '/devname.ini')
        if unicode(opt, 'utf-8') in self.devname.keys():
            conf.remove_option('devlist', opt)
            conf.write(open(sys.path[0] + '/devname.ini', 'w'))
            self.readdevname()
            self.set_runstate()
            return u"(_devid:%s)删除成功" % unicode(opt, 'utf-8')
        else:
            raise RuntimeError, u'(_devid:%s)不存在' % unicode(opt, 'utf-8')# }}}

    @property
    def devlist(self):
        dl = []
        for k in self.devname:
            dl.append({'_devid':k,\
                    '_type':self.devname[k].split(',')[0],\
                    '_conn':{'port':self.eviron['serial'].split(',').index(self.devname[k].split(',')[1]),\
                    'devaddr':self.devname[k].split(',')[2]}\
                    })
        return dl

    def connect(self):
        return mqtt.Mosquitto.connect(self, self.eviron['_server_ip'], int(self.eviron['_server_port']), int(self.eviron['_keepalive']))

    def publish(self, payload=None, retain=False):
        return mqtt.Mosquitto.publish(self, self.pubTopic, payload=payload, qos=1, retain=retain)

    def subscribe(self):
        return mqtt.Mosquitto.subscribe(self, self.subTopic, qos=int(self.eviron['subscribe_qos']))

    def cmdackpub(self, cmd_key, retcontent=None, errcode=0):
        try:
            if cmd_key in self.cmd_msg:
                msgd = self.cmd_msg.pop(cmd_key)
                send = self.make_pub_resp_msg(msgd, retcontent, errcode)
                if 'websocket' in msgd:
                    msgd['websocket'].send(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
                else:
                    self.publish(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
            elif cmd_key == 'write':
                for key in self.cmd_msg.keys():
                    if re.search(r',write$', key) != None:
                        msgd = self.cmd_msg.pop(key)
                        send = self.make_pub_resp_msg(msgd, retcontent, errcode)
                        if 'websocket' in msgd:
                            msgd['websocket'].send(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
                        else:
                            self.publish(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
        except Exception,e:
            self._error(e.message)

    def mqtt_message(self, mosq, obj, msg):
        self.base_message(mosq=mosq, obj=obj, msg=msg)

    def base_message(self, mosq=None, obj=None, msg=None, wsmsg=None , wsobj=None):    # {{{
        if msg or wsmsg:
            try:
                now_msg = {}
                if wsmsg:
                    now_msg = json.loads(wsmsg)
                elif (msg.qos == int(self.eviron['subscribe_qos']) and not msg.retain):
                    now_msg = json.loads(msg.payload)
                cmd, data = self.get_date_from_msg(now_msg)
                if cmd in (r'do/getvar', r'do/setvar'):
                    cw = 0
                    if cmd == r'do/setvar':
                        cw = True
#                    gevent.spawn(self.read_write_dev,data,now_msg,wsobj,cw).start
                    self.mkjob(1,self.read_write_dev,data,now_msg,wsobj,cw)
                else:
                    ret = self.sys_setget(cmd, data)
                    self.get_connect_info()
                    if isinstance(ret, (list, dict ,int)):
                        send_ack = self.make_pub_resp_msg(now_msg, ret, 0)
                    else:
                        send_ack = self.make_pub_resp_msg(now_msg, ret, 1)
#                     print json.dumps(send_ack,ensure_ascii=False,indent=1,encoding='gbk')
                    if wsmsg:
                        wsobj.send(json.dumps(send_ack, ensure_ascii=False, indent=1, encoding='utf-8'))
                    else:
                        self.publish(json.dumps(send_ack, ensure_ascii=False, indent=1, encoding='utf-8'))
                # self.publish(encode(send_ack))
            except Exception,e:
                self._error(e.message)

#                 self.publish(encode(self.make_pub_resp_msg(messdic,None,self.error_code(ret))))# }}}

    def read_write_dev (self,data,msg = None,wsob = None,write = None):
        dev = data
        retl = []
        statuscode = 1
        if not isinstance(dev,(list,)):
            dev = [dev]
        for ddic in dev:
            if ddic.has_key('_devid'):
                d = ddic['_devid']
                if isinstance(d ,(str,)):
                    d = unicode(d,'utf-8')
                if d in self.devname:
                    try:
                        dtype,port,addr = self.devname[d].split(',')
                        if write:
                            if ddic.has_key('_varname') and ddic.has_key('_varvalue'):
                                dvalue =  self.devtypes[dtype].write_dev_value(addr,port,ddic['_varname'],ddic['_varvalue'])
                                if isinstance(dvalue,dict):
                                    dvalue.update({"_devid":d})
                                else:
                                    dvalue = {d:dvalue}
                                retl.append(dvalue)
                                if isinstance(dvalue,(dict,int,list,unicode,str)):
                                    statuscode = 0
                            else:
                                retl.append(u'错误的请求数据:%r'%ddic)
                        else:
                            dvalue = self.devtypes[dtype].read_dev_value(addr,port)
                            if isinstance(dvalue,dict):
                                dvalue.update({"_devid":d})
                            else:
                                dvalue = {d:dvalue}
                            retl.append(dvalue)
                            if isinstance(dvalue,(dict,int,list,unicode,str)):
                                statuscode = 0
                    except Exception:
                        retl.append(u'设备:%s操作失败'%d)
                else:
                    retl.append(u'未知设备!%s'%d)
#                    raise ValueError,u'未知设备!%s'%d
            else:
                retl.append(u'错误的请求数据:%r'%ddic)
        if len(retl) == 1:
            retl = retl[0]
        if msg:
            send = self.make_pub_resp_msg(msg,retl,statuscode)
            if wsob:
                wsob.send(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
            else:
                self.publish(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
        return retl

    def mkjob(self,priority, jobfn,*args,**kwargs):
        class Job(object):
            def __init__(self, priority, jobfn,*args,**kwargs):
                self.priority = priority
                self.fak = jobfn,args,kwargs
            def __cmp__(self, other):
                return cmp(self.priority, other.priority)
            def run(self):
                return self.fak[0](*self.fak[1],**self.fak[2])
        self.pqueue.put(Job(priority, jobfn,*args,**kwargs))

    def onerd(self,d):
        time.sleep(0.1)
        dtype,port,addr = self.devname[d].split(',')
        pubmsg = self.devtypes[dtype].read_dev_value(addr,port)
        if isinstance(pubmsg,dict):
            pubmsg.update({"_devid":d})
        else:
            pubmsg = {d:pubmsg}
        pdata = self.make_pub_update_msg(pubmsg)
        if True:# (d,pubmsg) not in self.vardict.items():
            self.vardict.update({d:pubmsg})
            self.publish(json.dumps(pdata, ensure_ascii=False, indent=1, encoding='utf-8'))
            for ws in self.users:
                ws.send(json.dumps(pdata, ensure_ascii=False, indent=1, encoding='utf-8'))

    def autoread(self):
        while True:
            if not self.vardict:time.sleep(2)
            serdev = {}
            for ser in  self.eviron['serial'].split(','):
                serdev[ser] = []
                for d in self.devname:
                    if re.search(ser,self.devname[d]):
                        serdev[ser].append(d)
            for i in xrange(max([len(serdev[x]) for x in serdev])):
                serth = []
                for sd in serdev:
                    if serdev[sd] and i < len(serdev[sd]):
                        serth.append(gevent.spawn(self.onerd,serdev[sd][i%len(serdev[sd])]))
#                         serth = threading.Thread(target = self.onerd,args = (serdev[sd][i%len(serdev[sd])],))
                gevent.joinall(serth)
                while not self.pqueue.empty():
                    runcmd = self.pqueue.get()
                    runcmd.run()
            for sect in xrange(int(self.eviron['_interval']) * 10):
                while  not self.pqueue.empty():
                    runcmd = self.pqueue.get()
                    runcmd.run()
                time.sleep(0.1)

    def resetdefip(self):
        time.sleep(2)
        if gpio.read(self.GPIO_RESET) == 0:
            time.sleep(5)
            if gpio.read(self.GPIO_RESET) == 0:
                import shutil
                shutil.copyfile(sys.path[0] + '/interfaces', '/etc/network/interfaces')
                gpio.set(self.GPIO_RUN,1)
                os.popen('reboot')
        gevent.spawn(self.resetdefip).join()

    def led_link(self,gp,delay):
        if self.loop() == 0:
            delay = 1
        else:
            delay = 0.2
        time.sleep(delay)
        v = gpio.read(gp)
        if v:
            gpio.set(gp,0)
        else:
            gpio.set(gp,1)
        gevent.spawn(self.led_link,gp,delay).join

    def led_run(self,gp,delay):
        time.sleep(delay)
        v = gpio.read(gp)
        if v:
            gpio.set(gp,0)
        else:
            gpio.set(gp,1)
        gevent.spawn(self.led_run,gp,delay).join

if __name__ == '__main__':
    try:
        yjiongApp = YjApp(yj_global.CONFIGPATH, __name__)
        yjiongApp.load_drive(sys.path[0] + '/device')

        @yjiongApp.route('/')
        def index():
            return render_template('client.html', lip=yjiongApp.eviron["external_ip"])

        @yjiongApp.route('/message')
        def message(): # {{{
            if request.environ.get('wsgi.websocket'):
                ws = request.environ['wsgi.websocket']
                yjiongApp.users.add(ws)
                try:
                    while not ws.closed:
                        message = ws.receive()
                        if message:
                            try:
                                yjiongApp.base_message(wsmsg=message, wsobj=ws)
                            except WebSocketError:
                                print  sys.exc_info()
                                print "no user"
                    yjiongApp.users.remove(ws)
                    print 'Expected WebSocket request.'
                except:
                    yjiongApp._error(sys.exc_info()[1])
                    print  sys.exc_info()# }}}
            return "byebye"

        wsserver = WSGIServer(("0.0.0.0", 8000), yjiongApp, handler_class=WebSocketHandler)
        gevent.joinall( [gevent.spawn(run, (yjiongApp)),
            #gevent.spawn(apprun()),
            gevent.spawn(yjiongApp.autoread),
            gevent.spawn(wsserver.serve_forever)
            ])
    except Exception,e:
        print sys.exc_info()
        YjApp._error(e.message)



