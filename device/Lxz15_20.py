#!/usr/bin/python
#encoding:UTF-8
import base
import sys,time,json,time
from base import DynApp,DevObj 
from yj_global import yj_debugging
#from itertools import count
from MyError import *
_debug = 0
#_log = base.ModuleLogger(globals())

def int2bcd(a):
    '''int to bcd code'''
    return (a>>4)*10+ (a & 0x0f)

def bcd2int(a):
    '''bcd code to int'''
    return hex(((a / 10)<<4)+(a % 10))

@DynApp.registerdev(DynApp,'LXZ-15E~20E')
@yj_debugging
class Lxz15_20(DevObj):
    def __init__(self):
        self.set_config()
        if _debug: self._logger.setLevel(10);self._debug("__init__ ")
################################---user code---################################
        self.head_code = [0xFE,0xFE,0xFE,0x68]
        self.metertype = 0x10
        self.factory_code = 0x20
        self.ver_code = 0
        self.crl_code = {'read':0x01,'write':0x04,'read_addr':0x03,'write_addr':0x15,'write_flow':0x16}
        self.DI0_1 = {'read_all':[0x90,0x1E],
                      'read_ver':[0x81,0x3F],
                      'read_production_date':[0x81,0x3B],
                      'read_addr':[0x81,0x0a],
                      'read_price':[0x81,0x02],
                      'read_recharge':[0x81,0x05],
                      'write_time':[0xA0,0x15],
                      'open':[0xA0,0x17,0x55],
                      'close':[0xA0,0x17,0x99],
                      'initialize':[0xA0,0x20],
                      'write_recharge':[0xA0,0x13],
                      'write_price':[0xA0,0x10],
                      'write_addr':[0xA0,0x18],
                      'write_pflow':[0x90,0x1F],
                      'write_nflow':[0x90,0x1E]} 
        self.encryptl = [7,13,8,10,14,12,3,11,1,6,4,0,5,15,9,2] 
               
    @classmethod
    def set_config(cls):
        cls.baudrate = 2400
        cls.bytesize = 8
        cls.parity = 'N'
        cls.stopbits = 1
        cls.timeout = 1 
        
################################---user code---################################
    def sum2(self,val): 
        rsum = 0
        for summ in val:
            rsum = (rsum + summ) % 256
        return rsum
    def encrypt(self,value):
        def enc(va):
            l = self.encryptl.index(va & 0xf)
            h = self.encryptl.index(va >> 4) * 0x10
            return h + l
        return [enc(val) for val in value]    
#         for val in value:
#             yield enc(val)
            
    def iltoreal(self,il):
        rda = 0.0
#         print bin(il[-1:][0])[2]
        for da in il[::-1]:
            rda = rda * 0x100 + da
#         print int(il[-1:][0]) & 0x80
        if int(il[-1:][0]) & 0x80 == 0x80:
            rda = rda - 0x100000000  
#         print rda
        return rda/100.0
    
    def create_buf(self,amm_addr,ctl = 'read',DI = 'read_all',val = []):
        amm_addr = amm_addr.zfill(10)
        buf = self.head_code + [self.metertype]
        buf.append(int(amm_addr[8:10]) / 10 * 16 + int(amm_addr[8:10]) % 10)
        buf.append(int(amm_addr[6:8]) / 10 * 16 + int(amm_addr[6:8]) % 10)
        buf.append(int(amm_addr[4:6]) / 10 * 16 + int(amm_addr[4:6]) % 10)
        buf.append(int(amm_addr[2:4]) / 10 * 16 + int(amm_addr[2:4]) % 10)
        buf.append(int(amm_addr[0:2]) / 10 * 16 + int(amm_addr[0:2]) % 10)
        buf += [self.factory_code]
        buf.append(self.sum2(buf[5:]))
        buf += [self.ver_code]
        buf += [self.crl_code[ctl]]
        buf += [len(self.DI0_1[DI]) + len(val)]
        buf += self.DI0_1[DI]
        buf += val
        buf.append(self.sum2(buf[3:]))
        buf += [0x16]
        return buf

    def write_data(self,ser,addr,r_w,di0_1,value = []):
        try:
            write_data = self.create_buf(addr,r_w,di0_1,value)
#             print 'the write data is:',[hex(t) for t in write_data]  
            rece_data = []
            time.sleep(0.1)
            ser.write(write_data)
            for k in xrange(60):
                rs = ser.read(1)
                intva = ord(rs)
                rece_data += [intva] 
                if 0x68 in rece_data and\
                        0x16 == rece_data[len(rece_data)-1] and\
                        self.sum2(rece_data[rece_data.index(0x68):len(rece_data)-2]) == rece_data[len(rece_data)-2]:
                        break
            return rece_data[rece_data.index(0x68):]
        except:
            return sys.exc_info()[1]
             
    def read_version(self,ser,addr):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            tdata = self.write_data(ser,addr,'read','read_ver')
            if isinstance(tdata,list):
                ret = str(tdata[tdata.index(0x68)+14]/10.0 + tdata[tdata.index(0x68)+14] / 100.0)
            else:
                ret = tdata
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret   
            
    def read_production_date(self,ser,addr):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            tdata = self.write_data(ser,addr,'read','read_production_date')
            if isinstance(tdata,list):
                ret = '%d-%d-%r'%(tdata[17]*0x100 + tdata[16],int2bcd(tdata[15]),int2bcd(tdata[14]))
            else:
                ret = tdata
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret  
    
    def read_price(self,ser,addr):
        ret = 0
        try:
            tdata = self.write_data(ser,addr,'read','read_price')
#             print [hex(t) for t in tdata]
            if isinstance(tdata,list):
#                 print [hex(t) for t in tdata[14:32]]
                price_1 = self.iltoreal(tdata[14:16]);use_1 = int(self.iltoreal(tdata[16:18]) * 100)
                price_2 = self.iltoreal(tdata[18:20]);use_2 = int(self.iltoreal(tdata[20:22]) * 100)
                price_3 = self.iltoreal(tdata[22:24])#;use_3 = int(self.iltoreal(tdata[24:26]) * 100)
#                 print 'price1=%.2f,price2=%.2f,price3=%.2f'%(price_1,price_2,price_3)
#                 print 'use1=%d,use2=%d,use3=%d'%(use_1,use_2,use_3)
                tret = '{"price_1":%0.2f,"price_2":%0.2f,"price_3":%0.2f,"use_1":%d,"use_2":%d}'%(price_1,price_2,price_3,use_1,use_2)
                ret = json.loads(tret)
            else:
                ret = tdata
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret  
####################################    该表不支持此命令    #########################################  
    def read_recharge(self,ser,addr):
        ret = 0
        try:
            tdata = self.write_data(ser,addr,'read','read_recharge')
            if isinstance(tdata,list):
                print [hex(t) for t in tdata]
            else:
                ret = tdata
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret  
######################################################################################################   
                        
    def read_device(self,addr,ser,xuhao = False,syje = False):
        if _debug: self._debug(r"read_device   addr:%-10rport:%r"%(addr,ser.port))
        device_value = {}
        try:
#            while rcount > 0 and len(rece_data) == 0:
#            rcount -= 1
            send_data = self.create_buf(addr)
            ser.write(send_data)
#            time.sleep(0.1)
            rece_data = []
            for k in xrange(60):
                rs = ser.read(1)
                intva = ord(rs)
                rece_data += [intva] 
                if 0x68 in rece_data and\
                      0x16 == rece_data[len(rece_data)-1] and\
                        self.sum2(rece_data[rece_data.index(0x68):len(rece_data)-2]) == rece_data[len(rece_data)-2]:
                        break
            if self.sum2(rece_data[rece_data.index(0x68):len(rece_data)-2]) == rece_data[len(rece_data)-2]:
                valve_s = ['open','close']
                battery_s = ['normal','undervoltage']
                ds = rece_data.index(0x68) + 14
                zhengxiangliuliang = self.iltoreal(rece_data[ds:ds+4])
                fanxiangliuliang = self.iltoreal(rece_data[ds+5:ds+9])
                shengyujine = self.iltoreal(rece_data[ds+10:ds+14])
                shunshiliuliang = self.iltoreal(rece_data[ds+14:ds+17])
                shijian = r'%d-%d-%d %d:%d:%d'%(rece_data[ds+24] * 0x100 + rece_data[ds+23],rece_data[ds+22],rece_data[ds+21],rece_data[ds+20],rece_data[ds+19],rece_data[ds+18])
                zhuangtaizi = rece_data[ds+25:ds+29]
                valve_status = valve_s[(int(zhuangtaizi[0]) & 0x1)]
                mbattery_status = battery_s[(int(zhuangtaizi[0]) & 0x2)/0x2]
                bbattery_status = battery_s[(int(zhuangtaizi[0]) & 0x4)/0x4]
                goumaixuhao = rece_data[ds+29]
#                 print '正向累计流量:%-30.2f%-r'%(zhengxiangliuliang,[hex(h) for h in rece_data[ds:ds+4]])
#                 print '反向累计流量:%0-30.2f%-r'%((fanxiangliuliang),[hex(h) for h in rece_data[ds+5:ds+9]])
#                 print '剩余金额    :%-30.2f%-r'%((shengyujine),[hex(h) for h in rece_data[ds+10:ds+14]])
#                 print '瞬间流量    :%-30.2f%-r'%((shunshiliuliang),[hex(h) for h in rece_data[ds+14:ds+18]])
#                 print '时间        :%-30r%-r'%((shijian),[hex(h) for h in rece_data[ds+18:ds+25]])
#                 print '状态字      :%-30r%-r'%((zhuangtaizi),[hex(h) for h in rece_data[ds+25:ds+29]])
#                 print '阀门      :%-30r'%valve_status
#                 print '主电池状态      :%-30r'%mbattery_status
#                 print '备用电池状态     :%-30r'%bbattery_status
#                 print '购买序号    :%-30d%-r'%((goumaixuhao), hex(goumaixuhao))
                if not xuhao and not syje:
                    device_value.update({'_p_toltal_flow':zhengxiangliuliang,
                                     '_n_toltal_flow':fanxiangliuliang,
                                     '_remaining_money':shengyujine,
                                     '_moment_flow':shunshiliuliang,
                                     '_localtime':shijian,
#                                     '_statu_word':zhuangtaizi,
                                    '_valve_status':valve_status,
                                    '_mbattery_status':mbattery_status,
                                    '_bbattery_status':bbattery_status,
                                    '_production_date':self.read_production_date(ser, addr),
                                    '_version':self.read_version(ser,addr),
                                    '_price':self.read_price(ser,addr)
#                                      '_buy_Serial_number':goumaixuhao
                                     })
    #         print  ser.read(ser.in_waiting)
            if _debug: self._debug(r'the device data is %r'%device_value)
        except:
            print sys.exc_info()[1]
        finally:
            if xuhao:return goumaixuhao
            if syje:return {'_remaining_money':shengyujine}
            else:return device_value 


    def set_time(self,ser,addr,var_value):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            if isinstance(var_value, dict) and var_value.has_key('date') and var_value.has_key('time'):
            # var_value format :{'date':'12/31/2016','time':'10:35:55'}
                import re
                pattern_1 = re.compile(r'(\d{1,2})/(\d{1,2})/\d{2}(\d{2})')
                pattern_2 = re.compile(r'(\d{1,2}):(\d{1,2}):(\d{1,2})')            
                if  re.match(r'\d{1,2}/\d{1,2}/\d{4}',var_value['date']) and re.match(r'\d{1,2}:\d{1,2}:\d{1,2}',var_value['time']):
                    t1 = pattern_1.match(var_value['date']).groups()
                    t2 = pattern_2.match(var_value['time']).groups()
                    val = list(t2[::-1]) + list(t1[:2][::-1]) + [(2000+int(t1[2]))%0x100,(2000+int(t1[2]))/0x100]
                    val = [int(v) for v in val]
                    write_data = self.create_buf(addr,'write','write_time',val)
                    time.sleep(0.1)
                    ser.write(write_data)
                    rece_data = []
                    for k in xrange(60):
                        rs = ser.read(1)
                        intva = ord(rs)
                        rece_data += [intva] 
                        if 0x68 in rece_data and\
                                0x16 == rece_data[len(rece_data)-1] and\
                                self.sum2(rece_data[rece_data.index(0x68):len(rece_data)-2]) == rece_data[len(rece_data)-2]:
                                break
                    if self.sum2(rece_data[rece_data.index(0x68):len(rece_data)-2]) == rece_data[len(rece_data)-2]:pass
                    else:ret = 'set_time error'     
                else:ret = 'wrong format'
            else:ret = 'wrong data'
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret
    
    def ctr_valve(self,ser,addr,var_value):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            tdata = self.write_data(ser,addr,'write',var_value)
            if isinstance(tdata,list):
                ret = 0
            else:
                ret = 'wrong data'
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret 
    
    def initialize(self,ser,addr,var_value):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            tdata = self.write_data(ser,addr,'write','initialize')
            if isinstance(tdata,list):
                ret = 0
            else:
                ret = 'wrong data'
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret 
 
    def recharge(self,ser,addr,var_value):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            xuhao = self.read_device(addr,ser,True)
            if not isinstance(var_value, (int,long,float)):
                return 'wrong data'
            hvar = long(var_value * 100) 
            t = [int(hvar & 0xff),int((hvar & 0xff00) >> 8),int((hvar & 0xff0000) >> 16),int((hvar & 0xff000000) >> 24)]
#             print [hex(x) for x in t],hex(hvar)
            tl = [xuhao] + self.encrypt(t)
            tl.append(self.sum2(t))
            tl += [0x34,0x12,0x78,0x56]
            tdata = self.write_data(ser,addr,'write','write_recharge',tl)
#             for i in xrange(0,0x10):
#                 st = 0xb0 + i
#                 for j in xrange(0x10):
#                     xuhao = self.read_device(addr,ser,True)
#                     tl = [xuhao,st,0xBB,0xBB,0xBB,j,0x34 ,0x12,0x78 ,0x56]
#                     #[F5 BB BB BB BB 00 34 12 78 56]发送充值0元命令，充值序号0XF5，金额解密后的累加和为00，操作人员1234，充值设备5678
#                     tdata = self.write_data(ser,addr,'write','write_recharge',tl)
#                     if [hex(ttt) for ttt in tdata][14] == '0x12':
#                         print i,j
#                         break
            if isinstance(tdata,list) and hex(tdata[14]) == '0x12':
                time.sleep(1)
                ret = self.read_device(addr,ser,0,1)
            else:
                ret = 'wrong data'
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret 

    def modfi_price(self,ser,addr,var_value):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        try:
            if not isinstance(var_value, dict):
                return 'wrong data'
            def dtos(val):
                tval = long(val * 100) 
                return [int(tval & 0xff),int((tval & 0xff00) >> 8)]
            efts = [01,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA]          
            p1 = dtos(var_value['price_1']) 
            p2 = dtos(var_value['price_2']) 
            p3 = dtos(var_value['price_3']) 
            u1 = [int(long(var_value['use_1']) & 0xff),int((long(var_value['use_1']) & 0xff00) >> 8)]
            u2 = [int(long(var_value['use_2']) & 0xff),int((long(var_value['use_2']) & 0xff00) >> 8)]
            alu1 = [int(long(var_value['alread_used_1']) & 0xff),int((long(var_value['alread_used_1']) & 0xff00) >> 8)]
            alu2 = [int(long(var_value['alread_used_2']) & 0xff),int((long(var_value['alread_used_2']) & 0xff00) >> 8)]            
            tl = p1 + u1 + p2 + u2 + p3 + efts + alu1 + alu2
#             print [hex(ttt)  for ttt in tl]
            tdata = self.write_data(ser,addr,'write','write_price',tl)
            if isinstance(tdata,list) and tdata[14:18] == [0,0,0,0]:
                ret = 0
            else:
                ret = 'wrong data'
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret            
           
    def write_flow(self,ser,addr,p_n,var_value):
        ret = 0         #ret为返回值，0代表命令执行成功，如果不成功，就返回错误信息。
        if not isinstance(var_value , (int,long,float)):
            return "wrong data"
        try:
            d = long(var_value * 100000)
            tl = [int(t) for t in [d & 0xff,(d & 0xff00) >> 8,(d & 0xff0000) >> 16,(d & 0xff000000) >> 24]]
            tl.append(0x29)
            tdata = self.write_data(ser,addr,'write_flow',p_n,tl)
            if isinstance(tdata,list):
                ret = 0
            else:
                ret = 'wrong data'
        except:
            ret = 'wrong data'
            print sys.exc_info()[1]
        if _debug: self._debug(r'the return code is %r'%ret)
        return ret     
    
    def write_pflow(self,ser,addr,var_value):
        return self.write_flow(ser,addr,'write_pflow',var_value)
    
    def write_nflow(self,ser,addr,var_value):
        return self.write_flow(ser,addr,'write_nflow',var_value)
################################---user code---################################

   
if __name__ == '__main__':
    _debug = 0
    mydev = Lxz15_20()
    myser = mydev._getser("/dev/ttyUSB0",0) 
    addr = '1612230290'
#    print mydev.read_dev_value(addr,"/dev/ttyUSB0") 
#    print mydev.write_dev_value(addr,"/dev/ttyUSB0",'set_time',{'date':'5/22/2017','time':'14:29:40'})      
#    value = 123
#    mydev.set_time(myser, addr, {'date':'5/22/2017','time':'15:29:40'})
#    print mydev.ctr_valve(myser, addr, 'open')
#    print mydev.read_version(myser, addr)
#    print mydev.read_production_date(myser, addr)
#    mydev.write_pflow(myser, addr, 0.18)
#    mydev.initialize(myser, addr, '')
#    print mydev.recharge(myser, addr,0.3)
#    price = {'price_1': 5.2,  'price_2': 5.3, 'price_3': 5.4, 'use_1': 33,'use_2': 55,'alread_used_1':4,'alread_used_2':8}
#    mydev.modfi_price(myser,addr,price)
#    print mydev.read_price("/dev/ttyUSB0", addr)
#    print mydev.read_recharge(myser, addr)

    print json.dumps(mydev.read_device(addr,myser),encoding='utf-8',ensure_ascii=False,indent=1)
    
